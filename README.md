# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains the source code for a MATLAB simulator of the Soccer 3D environment.
This simulator was initally developed for solving the cooperative self-localization problem, as published in the paper *Cooperative Localization for Multiple Soccer Agents using Factor Graphs and Sequential Monte Carlo*: https://ieeexplore.ieee.org/document/9269974

* Version: 1.0.0

### How do I get set up? ###

Run this code directly using MATLAB/Octave. It was written on Matlab R2015b.
 
#### Simulation #####
* Methods for scenario generation and performing the filtering are located in the Humanoid class, *Humanoid.m*
* Run the simulation in the script *simulateMultiplayerMonteCarloTrajectory.m*, which instantiates the team of R Humanoids.

#### Results ####
* The simulation script saves a bunch of relevant information from the simulation. There are other scripts to consolidate all output files into simpler ones and to generate some relevant plots and tables:
* *output_final/renate_files.m* renames files with different IDs to a single ID.
* *processAllMatFiles.m* opens all .mat files with a given ID and saves the relevant data into a single simID.mat file.
* *plotsPaper.m* and *plotsAllRobotsAndMonteCarlo.m* generate relevant plots and tables, some of them shown in the paper.
* *plotCpuTime.m* generates table values for the CPU time of each method.

### Contribution guidelines ###

Please contact the author (Guilherme Fernandes) or the collaborators mentioned below

### Who do I talk to? ###

* Guilherme Fernandes: guilhermecgf@gmail.com
* Stiven Dias: stivendias@gmail.com
* Marcos Maximo: marcos.maximo@gmail.com