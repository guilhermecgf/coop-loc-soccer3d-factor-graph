function [goalAreaCornerList] = initializeGoalAreaCorners(field)
    fieldLength = field.fieldLength;     
    goalAreaLength = field.goalAreaLength;
    goalAreaWidth = field.goalAreaWidth;

    goalAreaCornerList=zeros(4,2);
    goalAreaCornerList(1,:)=[ (fieldLength/2-goalAreaLength),  goalAreaWidth/2];
    goalAreaCornerList(2,:)=[ (fieldLength/2-goalAreaLength), -goalAreaWidth/2];
    goalAreaCornerList(3,:)=[-(fieldLength/2-goalAreaLength),  goalAreaWidth/2];
    goalAreaCornerList(4,:)=[-(fieldLength/2-goalAreaLength), -goalAreaWidth/2];
end
