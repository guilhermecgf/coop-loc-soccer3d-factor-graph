function [lineList] = initializeLines(field)
    fieldLength = field.fieldLength;
    fieldWidth = field.fieldWidth;
    goalAreaWidth = field.goalAreaWidth;
    goalAreaLength = field.goalAreaLength;

    lineList=zeros(11, 4); %Line i => lineList(i, xMin, yMin, xMax, yMax)
    lineList(1, :)=[-fieldLength/2, fieldWidth/2, fieldLength/2, fieldWidth/2];
    lineList(2, :)=[-fieldLength/2, -fieldWidth/2, fieldLength/2, -fieldWidth/2];
    lineList(3, :)=[-fieldLength/2, -fieldWidth/2, -fieldLength/2, fieldWidth/2];
    lineList(4, :)=[fieldLength/2, -fieldWidth/2, fieldLength/2, fieldWidth/2];
    lineList(5, :)=[0, -fieldWidth/2, 0, fieldWidth/2];
    lineList(6, :)=[-fieldLength/2, -goalAreaWidth/2, -(fieldLength/2-goalAreaLength), -goalAreaWidth/2];
    lineList(7, :)=[-fieldLength/2, goalAreaWidth/2, -(fieldLength/2-goalAreaLength), goalAreaWidth/2];
    lineList(8, :)=[-(fieldLength/2-goalAreaLength), goalAreaWidth/2, -(fieldLength/2-goalAreaLength), -goalAreaWidth/2];
    lineList(9, :)=[fieldLength/2, -goalAreaWidth/2, (fieldLength/2-goalAreaLength), -goalAreaWidth/2];
    lineList(10, :)=[fieldLength/2, goalAreaWidth/2, (fieldLength/2-goalAreaLength), goalAreaWidth/2];
    lineList(11, :)=[(fieldLength/2-goalAreaLength), goalAreaWidth/2, (fieldLength/2-goalAreaLength), -goalAreaWidth/2];
end
