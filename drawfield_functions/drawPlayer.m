% Draws a player in the field, along with its local coordinate axes and
% angle of vision

function [] = drawPlayer(x,y,psi,visionAngle,ID,color)
%settings
pointSize = 5;
lineWidth = 1;

text(x+0.25,y-0.25,num2str(ID));
if nargin == 6
%     plot(x,y,'o','MarkerSize',pointSize,'MarkerFaceColor',color,'MarkerEdgeColor',color);
    plot(x,y,'o','MarkerSize',pointSize,'MarkerEdgeColor',color);
else
%     plot(x,y,'ok','MarkerSize',pointSize,'MarkerFaceColor','k');
    plot(x,y,'ok','MarkerSize',pointSize,'MarkerEdgeColor','k');
end

plot([x,x+1*cos(psi)], [y, y+1*sin(psi)], '-', 'Color', [0.3, 0.3, 0.3], 'LineWidth', lineWidth);
% plot([x,x+1*cos(psi+pi/2)], [y, y+1*sin(psi+pi/2)], '-', 'Color', [0.75, 0.75, 0.75], 'LineWidth', lineWidth);

if ID == 6
    hold on;
    d = 50;
    plot([x,x+d*cos(psi+visionAngle)],[y,y+d*sin(psi+visionAngle)],'--k','LineWidth',lineWidth);
    plot([x,x+d*cos(psi-visionAngle)],[y,y+d*sin(psi-visionAngle)],'--k','LineWidth',lineWidth);
    
    text(x + 1.25*cos(psi), y+1.25*sin(psi), 'x�')
    text(x + 1.25*cos(psi+pi/2), y+1.25*sin(psi+pi/2), 'y�')
end

end
