function [goalList] = initializeGoals(field)
    fieldLength = field.fieldLength;
    goalWidth = field.goalWidth;

    goalList=zeros(4,2); %Landmark i => landmarkList(i,x,y)
    goalList(1,:)=[-fieldLength/2, goalWidth/2];%Team's goal
    goalList(2,:)=[-fieldLength/2,-goalWidth/2];%Team's goal
    goalList(3,:)=[ fieldLength/2, goalWidth/2];%Opponent's goal
    goalList(4,:)=[ fieldLength/2,-goalWidth/2];%Opponent's goal
end
