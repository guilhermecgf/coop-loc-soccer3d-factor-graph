function displace = displacementInGlobal(state_prev, u)

    displace = zeros(3,1);
    displace(1) = u(1)*cos(state_prev(3) + u(3)/2) - u(2)*sin(state_prev(3) + u(3)/2);
    displace(2) = u(1)*sin(state_prev(3) + u(3)/2) + u(2)*cos(state_prev(3) + u(3)/2);
    displace(3) = u(3);

end