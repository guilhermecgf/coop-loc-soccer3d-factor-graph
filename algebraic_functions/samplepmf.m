function [ samples ] = samplepmf( pmf, n )
%samplepmf obtains n samples from a pmf described by "pmf"
%   inputs: 
%   - pmf, vector with dimension D. Should be normalized to unity sum.
%   - n, number of samples to be obtained.
%
%   output:
%   - samples: vector with dimension n. Its elements are integers from 1 to
%   D, sampled according to the input pmf
    if nargin < 2
        n = 1;
    end
    
    if sum(pmf) ~= 1
%         warning('input pmf does not sum to one')
    end
    
    samples = zeros(1,n);
    
    cumpmf = cumsum(pmf);
    
    % normalizing to sum one
    cumpmf = cumpmf/cumpmf(end);
    cumpmf(end) = 1;
    
    % 
    for i = 1:n
        aux = rand; % aux \in [0,1];
        cumpmf_aux = cumpmf - aux;
        indices = find(cumpmf_aux >= 0);
        samples(i) = indices(1);
    end

end

