% Algebraic functions used in the cooperative localization problem
% Type "state" is a vector with the pose with 3 DoF: [x, y, psi].
%
% rho12 = hRho(state1, state2)
% rho12 is the distance between agents in state1 and in state2.
function rho12 = hRho(state1, state2)
    rho12 = sqrt( (state2(2) - state1(2))^2 + (state2(1) - state1(1))^2 );
end