% Algebraic functions used in the cooperative localization problem
% Type "state" is a vector with the pose with 3 DoF: [x, y, psi].
%
% p = pStateDyn(state_curr, state_prev, u, Sigma)
% inputs:
% - state_curr: current state of an agent
% - state_prev: previous state of an agent
% - u: desired displacement, applied to state_prev
% - Sigma: odometry covariance matrix. Sigma = diag(var_x, var_y, var_psi)
%
% outputs:
% - p: evaluation of probability density defined by the inputs
%
% Random variables State_curr and State_prev relate each other according to
%
% State_curr = fStateDyn(State_prev, u) + W.
%
% The probability density is evaluated for State_curr = state_curr,
% conditioned on State_prev = state_prev.
function p = pStateDyn(state_curr, state_prev, u, Sigma)
    mu = fStateDyn(state_prev, u);
    
    % assuming normally distributed noise, with covariance matrix Sigma.
    % ALREADY CODED IN THE HUMANOID CLASS!!!
    % note that mvnpdf function uses the covariance matrix as argument!
    p = mvnpdf(state_curr, mu, Sigma);    
end