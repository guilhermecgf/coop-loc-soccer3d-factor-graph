% Algebraic functions used in the cooperative localization problem
% Type "state" is a vector with the pose with 3 DoF: [x, y, psi].
%
% phi12 = hPhi(state1, state2)
% phi12 is the angle, in local coordinates, of the agent in state2 with
% reference in the agent in state1
function phi12 = hPhi(state1, state2)
    phi12 = atan2( state2(2) - state1(2) , state2(1) - state1(1) ) - state1(3);
    phi12 = wrapAngle(phi12);
    
%     ratio = phi12/(2*pi);
%     wraps = floor(ratio);
%     
%     phi12 = phi12 - wraps*2*pi;
%     
%     its = 0;
%     while(phi12 > pi)
%         phi12 = phi12 - 2*pi;
% %         its = its+1;
% %         if mod(its,50) == 2
% %             phi12, its
% %         end
%     end
%     while(phi12 <= -pi)
%         phi12 = phi12 + 2*pi;
% %         its = its+1;
% %         if mod(its,50) == 2
% %             phi12, its
% %         end
%     end
end