% Wraps received angle to (-pi + center, +pi + center] interval
%

%function [wrapped1, wrapped2] = wrapAngle(angle, center)
function [wrapped] = wrapAngle(angle, center)

    if nargin < 2
        center = 0;
    end

    %wrapped1 = angle;
    %
    %for i = 1:length(wrapped1)
    %    while wrapped1(i) > pi + center
    %       wrapped1(i) = wrapped1(i) - 2*pi;
    %    end
	%
    %    while wrapped1(i) <= -pi + center
    %        wrapped1(i) = wrapped1(i) + 2*pi;
    %    end
    %end
        
    k = ceil( (angle+pi-center) /(2*pi) ) - 1;
    wrapped = angle - 2*pi*k;
    
end