% Algebraic functions used in the cooperative localization problem
% Type "state" is a vector with the pose with 3 DoF: [x, y, psi].
%
% state_predicted = fStateDyn(state_prev, u)
% state_predicted considers a kinematic model from "state_prev" when a
% desired displacement u = [dx; dy; dpsi] is applied to the agent.
% u is given in the local coordinate frame!
function state_predicted = fStateDyn(state_prev, u)
%     state_predicted = zeros(3,1);
%     state_predicted(1) = state_prev(1) + u(1)*cos(state_prev(3) + u(3)/2) - u(2)*sin(state_prev(3) + u(3)/2);
%     state_predicted(2) = state_prev(2) + u(1)*sin(state_prev(3) + u(3)/2) + u(2)*cos(state_prev(3) + u(3)/2);
%     state_predicted(3) = state_prev(3) + u(3);
    
    state_predicted = state_prev + reshape( displacementInGlobal(state_prev, u), [3,1] );

    %     state_predicted(3) = wrapAngle(state_predicted(3));
    % fazer wrap fora da fun��o, onde for necess�rio
end