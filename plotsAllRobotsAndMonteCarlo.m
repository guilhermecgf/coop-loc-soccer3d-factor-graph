%% Script to plot the plots desired in the paper.
clear all; close all

addpath('plotresults_functions')
% load('output_final/2020022-novomc/novomc-2627fev.mat');
% load('output_final/sim29/sim29.mat');
% load('output_final/sim31/sim31.mat');
% load('output_final/sim37/sim37.mat');
% load('output_final/sim41/sim41.mat');
% load('output_final/sim67.mat');
% load('output_final/sim71.mat');
% load('D:\OneDrive\sim61.mat');
load('D:\OneDrive\sim107.mat');
% load('output_final/sim0.mat');

% load('output_final/sim493.mat'); % one sample MPF, Np = 100
% load('output_final/sim983.mat'); % one sample MPF, Np = 400
% load('output_final/sim173.mat'); % one sample MPF, Np = 1000
% load('output_final/sim867.mat'); % one sample MPF, mean, Np = 400
% load('output_final/sim958.mat'); % one sample MPF, mean, Np = 400, T = 0.02

%% list of plots
bRMSEPos = true; % fig20
bRMSEPsi = true; % fig30
bErrPos = false; % fig40
bErrPsi = false; % fig50
bObsAllies = false; % fig60
bObsLandmarksCum = false; % fig70
bObsLandmarksMC = false; % fig71
bBlackoutRun = false; % fig72
bRMSETalker = false; % fig80
bNeffMC = false; % fig82
bNeffTime = false; % fig83
bMSEMC = false; % fig84
bCovMC = false; % fig85-89
bCovTime = false; % fig91-95
bRelativeDist = false; % fig99

%%

saveplots = false;

obs_landmarks_average = squeeze(mean(obs_landmarks_total,1));
obs_players_average = squeeze(mean(obs_players_total,1));

obs_landmarks_std = squeeze(std(obs_landmarks_total,[],1));
obs_players_std = squeeze(std(obs_players_total,[],1));

%% Convert to cell format
mse_dist = cell(R,4);
mse_psi = cell(R,4);

% Linear error, to verify if the estimates are biased
error_x = cell(R,4);
error_y = cell(R,4);
error_psi = cell(R,4);

for r = 1:R
    for s = 1:4
        mse_dist{r,s} = mse_dist_time_total(:,:,r,s);
        mse_psi{r,s} = mse_psi_time_total(:,:,r,s);
        
        error_x{r,s} = err_x_time_total(:,:,r,s);
        error_y{r,s} = err_y_time_total(:,:,r,s);
        error_psi{r,s} = err_psi_time_total(:,:,r,s);
    end
end

%% If desired, remove outliers
boolRemoveOutliers = false;
if boolRemoveOutliers
    % k is number of standard deviations from the mean; internally,
    % function treats with median and absolutw error
    k = 2;
    [mse_dist, mse_psi] = removeOutliers(mse_dist_time_total, mse_psi_time_total, err_x_time_total, err_y_time_total, err_psi_time_total, k, R, M, nIterations);
end

%% Another way of removing outliers: considering only MSE averaged over all time instants
boolRemoveOutliers = false;
if boolRemoveOutliers
    k = 4;
    [mse_dist, mse_psi, ind_no_outliers_dist, ind_no_outliers_psi] = removeOutliersNew(mse_dist, mse_psi, k);       
    ind_no_outliers_dist, ind_no_outliers_psi
end

%%
% Para plotar, vamos deixar o plot do PLHA por ultimo.
setupNamePlot = {'PL', 'PLH', 'PLA', 'PLHA'};
for r = 1:R
    aux = mse_dist{r,3};
    mse_dist{r,3} = mse_dist{r,4};
    mse_dist{r,4} = aux;
    
    aux = mse_psi{r,3};
    mse_psi{r,3} = mse_psi{r,4};
    mse_psi{r,4} = aux;
end

mse_dist_time_total_plot = mse_dist_time_total(:,:,:,[1,2,4,3]);
mse_psi_time_total_plot = mse_psi_time_total(:,:,:,[1,2,4,3]);

for r = 1:R
    aux = error_x{r,3};
    error_x{r,3} = error_x{r,4};
    error_x{r,4} = aux;
    
    aux = error_y{r,3};
    error_y{r,3} = error_y{r,4};
    error_y{r,4} = aux;
    
    aux = error_psi{r,3};
    error_psi{r,3} = error_psi{r,4};
    error_psi{r,4} = aux;
end

% swapping covariance and number of particles
cov_time_plot = cov_time(:,:,:,:,:,[1,2,4,3]);
% Np = max(max(Neff_time(:,:,1,1)));
% Neff_time_plot = Neff_time(:,:,:,[1,2,4,3])/Np;
Neff_time_plot = Neff_time(:,:,:,[1,2,4,3]);

%% Plotting all robots, error vs. time, all methods in one plot

linetype = {'--', '-.', '-', '-', ':'}; % per robot

xlim_param = [0, time_vec(end)];
ylim_dist_param = [0, 1.6];

if bRMSEPos

figure(20);
for r = 1:R
    
%     subplot(5,2,2*r-1);
    subplot(2,5,r);
    for s = 1:4
        mean_mse = mean( mse_dist{r,s}, 1);
        var_mse = var( mse_dist{r,s}, 0, 1);
        ax = gca;
        ax.ColorOrderIndex = s;

        plot(time_vec, sqrt(mean_mse), [linetype{r}], 'LineWidth', 1.5);
        hold all;
    end    
    xlim(xlim_param); ylim(ylim_dist_param)
    xlabel('Time (s)'); ylabel('Position RMSE (m)');
    legend(setupNamePlot);
    title(['Robot ' num2str(r)]);
    
%     subplot(5,2,2*r);
    subplot(2,5,r+5);
    if R == 1
        total_camera_obs = [obs_landmarks_average, obs_players_average];
    else
        total_camera_obs = [obs_landmarks_average(r,:); obs_players_average(r,:)].';
    end
    H = bar(time_vec, total_camera_obs, 'stacked');
    colors = [0, 0.75, 0; 0.75, 0, 0.75];
    for k = 1:2
        set(H(k), 'facecolor', colors(k,:), 'EdgeColor', 'none');
    end
    xlabel('Time (s)'); ylabel('Number of camera observations');
    legend('Landmarks', 'Players'); ylim([0,8]); xlim([0,time_vec(end)]);

end

if saveplots
    saveas(gcf, 'figures_temp/fig20.fig'), saveas(gcf, 'figures_temp/fig20.png'), saveas(gcf, 'figures_temp/fig20','epsc');
end

end

%% Plotting all robots, error vs. time, all methods in one plot

linetype = {'--', '-.', '-', '-', ':'}; % per robot

xlim_param = [0, time_vec(end)];
ylim_psi_param = [0, 0.7];

if bRMSEPsi

figure(30);
for r = 1:R
    
%     subplot(5,2,2*r-1);
    subplot(2,5,r);
    for s = 1:4
        mean_mse = mean( mse_psi{r,s}, 1);
        var_mse = var( mse_psi{r,s}, 0, 1);
        ax = gca;
        ax.ColorOrderIndex = s;

        plot(time_vec, sqrt(mean_mse), [linetype{r}], 'LineWidth', 1.5);
        hold all;
    end    
    xlim(xlim_param); ylim(ylim_psi_param)
    xlabel('Time (s)'); ylabel('Orientation RMSE (rad)');
    legend(setupNamePlot);
    title(['Robot ' num2str(r)]);
    
%     subplot(5,2,2*r);
    subplot(2,5,r+5);
    if R == 1
        total_camera_obs = [obs_landmarks_average, obs_players_average];
    else
        total_camera_obs = [obs_landmarks_average(r,:); obs_players_average(r,:)].';
    end
    H = bar(time_vec, total_camera_obs, 'stacked');
    colors = [0, 0.75, 0; 0.75, 0, 0.75];
    for k = 1:2
        set(H(k), 'facecolor', colors(k,:), 'EdgeColor', 'none');
    end
    xlabel('Time (s)'); ylabel('Number of camera observations');
    legend('Landmarks', 'Players'); ylim([0,8]); xlim([0,time_vec(end)]);

end

if saveplots
    saveas(gcf, 'figures_temp/fig30.fig'), saveas(gcf, 'figures_temp/fig30.png'), saveas(gcf, 'figures_temp/fig30','epsc');
end

end

%% Plot of linear error, in x, y, and psi
cell_to_be_plotted = error_x;
linetype = {'--', '-.', '-', '-', ':'}; % per robot

xlim_param = [0, time_vec(end)];
ylim_dist_param = [-0.35, 0.35];

if bErrPos

figure(40);
for r = 1:R
    
    subplot(2,5,r);
    for s = 1:4
        mean_mse = mean( cell_to_be_plotted{r,s}, 1);
        var_mse = var( cell_to_be_plotted{r,s}, 0, 1);
        ax = gca;
        ax.ColorOrderIndex = s;

        plot(time_vec, mean_mse, [linetype{r}], 'LineWidth', 1.5);
        hold all;
%         disp(
    end    
    xlim(xlim_param); ylim(ylim_dist_param)
    xlabel('Time (s)'); ylabel('Error x (m)');
    legend(setupNamePlot);
    title(['Robot ' num2str(r)]);
    
end

cell_to_be_plotted = error_y;
linetype = {'--', '-.', '-', '-', ':'}; % per robot

xlim_param = [0, time_vec(end)];
ylim_dist_param = [-0.35, 0.35];

for r = 1:R
    
    subplot(2,5,r+5);
    for s = 1:4
        mean_mse = mean( cell_to_be_plotted{r,s}, 1);
        var_mse = var( cell_to_be_plotted{r,s}, 0, 1);
        ax = gca;
        ax.ColorOrderIndex = s;

        plot(time_vec, mean_mse, [linetype{r}], 'LineWidth', 1.5);
        hold all;
    end    
    xlim(xlim_param); ylim(ylim_dist_param)
    xlabel('Time (s)'); ylabel('Error y (m)');
    legend(setupNamePlot);
    title(['Robot ' num2str(r)]);
    
end

if saveplots
    saveas(gcf, 'figures_temp/fig40.fig'), saveas(gcf, 'figures_temp/fig40.png'), saveas(gcf, 'figures_temp/fig40','epsc');
end

end

if bErrPsi

% Linear error psi
cell_to_be_plotted = error_psi;
linetype = {'--', '-.', '-', '-', ':'}; % per robot

xlim_param = [0, time_vec(end)];
ylim_dist_param = [-0.15, 0.15];

figure(50);
for r = 1:R
    
    subplot(2,5,r);
    for s = 1:4
        mean_mse = mean( cell_to_be_plotted{r,s}, 1);
        var_mse = var( cell_to_be_plotted{r,s}, 0, 1);
        ax = gca;
        ax.ColorOrderIndex = s;

        plot(time_vec, mean_mse, [linetype{r}], 'LineWidth', 1.5);
        hold all;
    end    
    xlim(xlim_param); ylim(ylim_dist_param)
    xlabel('Time (s)'); ylabel('Error psi (rad)');
    legend(setupNamePlot);
    title(['Robot ' num2str(r)]);
    
end

if saveplots
    saveas(gcf, 'figures_temp/fig50.fig'), saveas(gcf, 'figures_temp/fig50.png'), saveas(gcf, 'figures_temp/fig50','epsc');
end

end

%% For each robot, observed allies versus time

if bObsAllies

observedAllies = zeros(R, nIterations, R);
for r = 1:R
    for n = 3:3:nIterations
        % actually, data only contains info about the last MC run
%         detectedAllies = data{1}.TeamEstSave{r,4}.measurementCamera{n}.playersID;
        detectedAllies = data.TeamEstSave{r,4}.measurementCamera{n}.playersID;
        for d = detectedAllies
            observedAllies(d,n,r) = d;
        end
    end
end


observedAllies = squeeze(mean(obs_allies_detail,1));
for rprime = 1:R
    observedAllies(:,rprime,:) = squeeze(observedAllies(:,rprime,:)) + (rprime-1)*ones(R,nIterations); 
end


figure(60);
for r = 1:R
    subplot(1,5,r);
    plot(time_vec(3:3:end), squeeze(observedAllies(r,:,3:3:end)),'.');
    xlabel('Time (s)');
    ylabel('Observed allies (average over Monte Carlo)');
    title(['Robot ' num2str(r)]) 
    ylim([0,R]);
end

if saveplots
    saveas(gcf, 'figures_temp/fig60.fig'), saveas(gcf, 'figures_temp/fig60.png'), saveas(gcf, 'figures_temp/fig60','epsc');
end

end

%% Average and std, landmarks and players
% figure(78);
% for r = 1:R
%     subplot(2,5,r);
%     plot(time_vec, obs_landmarks_average(r,:)); 
%     hold on;
%     errorbar(time_vec(3:300:end), obs_landmarks_average(r,3:300:end), obs_landmarks_std(r,3:300:end));
%     xlim([0, time_vec(end)]); ylim([-1, 5]);
%     
%     subplot(2,5,r+5);
%     plot(time_vec, obs_players_average(r,:)); 
%     hold on;
%     errorbar(time_vec(3:300:end), obs_players_average(r,3:300:end), obs_players_std(r,3:300:end));
%     xlim([0, time_vec(end)]); ylim([-1, 5]);
% end

% figure(79);
% for r = 1:R
%     subplot(2,5,r);
%     boxplot(squeeze(obs_landmarks_total(:,r,3:300:end)))
% end

% number of observed landmarks on all monte carlo runs
count_observed_landmarks = zeros(5, nIterations, R);
for r = 1:R
    for i = 1:nIterations
        for o = 0:4
            count_observed_landmarks(o+1, i, r) = sum(obs_landmarks_total(:,r,i) == o);
        end
    end
end

if bObsLandmarksCum

figure(70);
for r = 1:R    
    subplot(1,5,r);
    area(time_vec(3:3:end),count_observed_landmarks(:,3:3:end,r)', 'LineStyle', 'None');
    xlabel('Time index of camera observation')
    ylabel('Accumulation of number of observed landmarks');
end

if saveplots
    saveas(gcf, 'figures_temp/fig70.fig'), saveas(gcf, 'figures_temp/fig70.png'), saveas(gcf, 'figures_temp/fig70','epsc');
end

end

% counting the maximum blackout length for each time instant, considering
% all Monte Carlo runs
ind_obscamera = 3:3:length(time_vec);
time_vec_obscamera = time_vec(ind_obscamera);
maxBlackoutLength = zeros(R, length(time_vec_obscamera));
blackoutLengths = zeros(M, length(time_vec_obscamera), R);
for r = 1:R
    for m = 1:M
        aux = countBlackout(squeeze(obs_landmarks_total(m,r,ind_obscamera)));
        blackoutLengths(m,:,r) = reshape(aux, [1, numel(aux)]); 
    end
    aux2 = blackoutLengths(:,:,r);
    aux3 = max(aux2,[], 1);
    maxBlackoutLength(r, :) = aux3;
end

if bObsLandmarksMC

figure(71);
for r = 1:R
    subplot(2,5,r);
    imagesc(squeeze(obs_landmarks_total(:,r,3:3:end)), [0,4]);
%     imagesc(squeeze(obs_landmarks_total_sorted(:,r,3:3:end)), [0,4]);
end

maxBlackout = max(max(maxBlackoutLength));

figure(71);
for r = 1:R
    subplot(2,5,r+5);
%     plot(time_vec_obscamera, maxBlackoutLength(r, :));
    plot(maxBlackoutLength(r, :));
    ylim([0,maxBlackout + 20]); xlim([0, length(time_vec_obscamera)]);
end

if saveplots
    saveas(gcf, 'figures_temp/fig71.fig'), saveas(gcf, 'figures_temp/fig71.png'), saveas(gcf, 'figures_temp/fig71','epsc');
end

end


if bBlackoutRun

figure(72);
for r = 1:R
    subplot(1,5,r);
    plot(time_vec_obscamera, maxBlackoutLength(r, :));
%     plot(maxBlackoutLength(r, :));
    ylim([0,maxBlackout + 20]); xlim([0, time_vec_obscamera(end)]);
end

if saveplots
    saveas(gcf, 'figures_temp/fig72.fig'), saveas(gcf, 'figures_temp/fig72.png'), saveas(gcf, 'figures_temp/fig72','epsc');
end

end


% number of observed players on all monte carlo runs
% count_observed_players = zeros(R, nIterations, R);
% for r = 1:R
%     for i = 1:nIterations
%         for o = 0:R-1
%             count_observed_players(o+1, i, r) = sum(obs_players_total(:,r,i) == o);
%         end
%     end
% end
% 
% figure(77);
% for r = 1:R    
%     subplot(1,5,r);
%     area(time_vec(3:3:end),count_observed_players(:,3:3:end,r)', 'LineStyle', 'None');
%     xlabel('Time index of camera observation')
%     ylabel('Accumulation of number of observed players');
% end


% % sorting the Monte Carlo realizations according to the average number of
% % landmarks over all time instants, per robot.
% 
% % finding the average landmarks per time, for each robot and monte carlo
% % run
% obs_landmarks_avg_time = zeros(M,R);
% for m = 1:M
%     for r = 1:R
%         obs_landmarks_avg_time(m,r) = mean(sign(obs_landmarks_total(m,r,:)));
%     end
% end
% 
% % for each robot, sorting the monte carlo runs according to the average
% % number of landmarks seen
% obs_landmarks_total_sorted = obs_landmarks_total;
% for r = 1:R
%     [~, isort] = sort(obs_landmarks_avg_time(:,r));
%     obs_landmarks_total_sorted(:,r,:) = obs_landmarks_total(isort,r,:);    
% end
% 
% figure(76);
% for r = 1:R
%     subplot(2,5,r);
% %     imagesc(squeeze(obs_landmarks_total_sorted(:,r,3:3:end)));
%     imagesc(squeeze(obs_landmarks_total_sorted(:,r,3:3:end)), [0,2]);
% end

%% Inspecting error of talker in hearing assimilation
% not very conclusive

if bRMSETalker

talkerSchedule = data.talkerSchedule;

% PLH and PLHA
indPLH = 2;
indPLHA = 4;
errorTalkerPLH = zeros(R, nIterations, M); % R listeners, time, monte carlo
errorTalkerPLHA = zeros(R, nIterations, M); % R listeners, time, monte carlo
for r = 1:R
    for m = 1:M
        for n = 2:2:nIterations
            talker = talkerSchedule(n);
            if r ~= talker
                aux = mse_dist{talker,indPLH};
                errorTalkerPLH(r,n,m) = aux(m,n);
                aux = mse_dist{talker,indPLHA};
                errorTalkerPLHA(r,n,m) = aux(m,n);
            end
        end
    end
end

ind_talker = cell(R,1);
for r = 1:R
    ind_talker{r} = (2*r):10:nIterations;
end

figure(80);
for r = 1:R
    subplot(2,5,r);
    for talker = 1:R
        aux = squeeze(errorTalkerPLH(r,ind_talker{talker},:));
        aux_mean = mean(aux, 2);
        plot(T*ind_talker{talker}, aux_mean); hold on;
    end
    hold off;
    xlabel('Time instant');
    ylabel('Position RMSE of talker, PLH (m)');
    ylim([0,2]);
%     title(['Listener: R' num2str(r)]);
    legend('R1', 'R2', 'R3', 'R4', 'R5');
end

for r = 1:R
    subplot(2,5,r+5);
    for talker = 1:R
        aux = squeeze(errorTalkerPLHA(r,ind_talker{talker},:));
        aux_mean = mean(aux, 2);
        plot(T*ind_talker{talker}, aux_mean); hold on;
    end
    hold off;
    xlabel('Time instant');
    ylabel('Position RMSE of talker, PLHA (m)');
    ylim([0,2]);
%     title(['Listener: R' num2str(r)]);
    legend('R1', 'R2', 'R3', 'R4', 'R5');
end

if saveplots
    saveas(gcf, 'figures_temp/fig80.fig'), saveas(gcf, 'figures_temp/fig80.png'), saveas(gcf, 'figures_temp/fig80','epsc');
end

end

%% Record of effective number of particles

if bNeffMC

figure(82);
for r = 1:R
    for s = 1:4
        subplot(4,5,(s-1)*R+r);
        imagesc(Neff_time_plot(:,:,r,s));
        title(['R' num2str(r) ', ' setupNamePlot{s}]);
    end
end

if saveplots
    saveas(gcf, 'figures_temp/fig82.fig'), saveas(gcf, 'figures_temp/fig82.png'), saveas(gcf, 'figures_temp/fig82','epsc');
end

end

if bNeffTime
    Neff_time_plot_avg = squeeze(mean(Neff_time_plot,1));
    figure(83);
    for r = 1:R
        for s = 1:4
            subplot(1,R,r);
            plot(time_vec, Neff_time_plot_avg(:,r,s)); hold all;
        end
        xlabel('Time (s)'); ylabel('Average Neff'); title(['Robot ' num2str(r)]);
    end
    
    if saveplots
        saveas(gcf, 'figures_temp/fig83.fig'), saveas(gcf, 'figures_temp/fig83.png'), saveas(gcf, 'figures_temp/fig83','epsc');
    end
    
    
end
        

%% Evaluating error and covariance for each of the monte carlo runs
color_lims = [0, 2; 0, 7.5; 0, 2; 0, 2; 0, 2];

if bMSEMC

for r = 1:R
    figure(84);
    for s = 1:4
        subplot(4,5, R*(s-1)+r);
        imagesc(sqrt(mse_dist_time_total(:,:,r,s)), color_lims(r,:));
        title(['MSE, R' num2str(r) ', ' setupNamePlot{s}]);
    end
end

if saveplots
    saveas(gcf, 'figures_temp/fig84.fig'), saveas(gcf, 'figures_temp/fig84.png'), saveas(gcf, 'figures_temp/fig84','epsc');
end

end

entry_name = {'cov-x', 'cov-y', 'cov-psi', 'cov-x-y', 'cov-x-psi', 'cov-y-psi'};
x_entry_ind = [1, 2, 3, 1, 1, 2];
y_entry_ind = [1, 2, 3, 2, 3, 3];

if bCovMC
    
for r = 1:R
    figure(84+r);
    for s = 1:4
        for i = 1:length(x_entry_ind)
            subplot(4,6,(s-1)*6 + i);
            x_e = x_entry_ind(i);
            y_e = y_entry_ind(i);
            % lembrar que cov_time est� na ordem PL, PLH, PLHA, PLA
            imagesc(squeeze(cov_time_plot(x_e, y_e, :, :, r, s))');
            title([entry_name{i}]);
            
            if i == 1                   
                ylabel(['R' num2str(r) ', ' setupNamePlot{s}]);
            end
        end

    end
    
    if saveplots
        saveas(gcf, ['figures_temp/' num2str(84+r) '.fig']), saveas(gcf, ['figures_temp/' num2str(84+r) '.png']), saveas(gcf, ['figures_temp/' num2str(84+r)],'epsc');
    end
    
end

end

if bCovTime
    entry_name = {'sqrt-cov-x', 'sqrt-cov-y', 'sqrt-cov-psi', 'cov-xy', 'cov-xpsi', 'cov-ypsi'};
    ylim_axis = [0, 1; 0, 1; 0, 0.3; -0.2, 0.2; -0.05, 0.05; -0.05, 0.05];
    cov_time_plot_avg = squeeze(mean(cov_time_plot,4));
    
    for r = 1:R
        figure(90+r);
        for i = 1:length(x_entry_ind)
%             subplot(5,6,(r-1)*6 + i);
            subplot(2,3,i);
            for s = 1:4
                x_e = x_entry_ind(i);
                y_e = y_entry_ind(i);
                % lembrar que cov_time est� na ordem PL, PLH, PLHA, PLA
                if i <=3
                    plot(time_vec, sqrt(squeeze(cov_time_plot_avg(x_e, y_e, :, r, s))')); hold on
                else
                    plot(time_vec, squeeze(cov_time_plot_avg(x_e, y_e, :, r, s))'); hold on
                end
                title(['Robot ' num2str(r) ', ' entry_name{i}]);
                ylim(ylim_axis(i,:));
            end

        end
    
        if saveplots
            saveas(gcf, ['figures_temp/' num2str(90+r) '.fig']), saveas(gcf, ['figures_temp/' num2str(90+r) '.png']), saveas(gcf, ['figures_temp/' num2str(90+r)],'epsc');
        end
    end
end
        
%% Verifying if the robots are keeping their formation

if bRelativeDist

relative_pos_gt = zeros(R,R,nIterations);

for r = 1:R-1
    for a = r+1:R
        alldists_x_sq = (gt_x_time(:,:,r) - gt_x_time(:,:,a) ).^2;
        alldists_y_sq = (gt_y_time(:,:,r) - gt_y_time(:,:,a) ).^2;
        alldists_range = sqrt(alldists_x_sq + alldists_y_sq);
        relative_pos_gt(r, a, :) = mean(alldists_range);
    end
end

relative_pos_PL = zeros(R,R,nIterations);
est_x_PL = gt_x_time + err_x_time_total(:,:,:,1);
est_y_PL = gt_y_time + err_y_time_total(:,:,:,1);
est_psi_PL = gt_psi_time + err_psi_time_total(:,:,:,1);

relative_pos_PLH = zeros(R,R,nIterations);
est_x_PLH = gt_x_time + err_x_time_total(:,:,:,2);
est_y_PLH = gt_y_time + err_y_time_total(:,:,:,2);
est_psi_PLH = gt_psi_time + err_psi_time_total(:,:,:,2);

relative_pos_PLA = zeros(R,R,nIterations);
est_x_PLA = gt_x_time + err_x_time_total(:,:,:,3);
est_y_PLA = gt_y_time + err_y_time_total(:,:,:,3);
est_psi_PLA = gt_psi_time + err_psi_time_total(:,:,:,3);

relative_pos_PLHA = zeros(R,R,nIterations);
est_x_PLHA = gt_x_time + err_x_time_total(:,:,:,4);
est_y_PLHA = gt_y_time + err_y_time_total(:,:,:,4);
est_psi_PLHA = gt_psi_time + err_psi_time_total(:,:,:,4);

for r = 1:R-1
    for a = r+1:R
        x1 = est_x_PL(:,:,r);
        y1 = est_y_PL(:,:,r);
        x2 = est_x_PL(:,:,a);
        y2 = est_y_PL(:,:,a);
        relative_pos_PL(r, a, :) = mean(sqrt( (x1-x2).^2 + (y1-y2).^2 ));
        
        x1 = est_x_PLH(:,:,r);
        y1 = est_y_PLH(:,:,r);
        x2 = est_x_PLH(:,:,a);
        y2 = est_y_PLH(:,:,a);
        relative_pos_PLH(r, a, :) = mean(sqrt( (x1-x2).^2 + (y1-y2).^2 ));
        
        x1 = est_x_PLA(:,:,r);
        y1 = est_y_PLA(:,:,r);
        x2 = est_x_PLA(:,:,a);
        y2 = est_y_PLA(:,:,a);
        relative_pos_PLA(r, a, :) = mean(sqrt( (x1-x2).^2 + (y1-y2).^2 ));
        
        x1 = est_x_PLHA(:,:,r);
        y1 = est_y_PLHA(:,:,r);
        x2 = est_x_PLHA(:,:,a);
        y2 = est_y_PLHA(:,:,a);
        relative_pos_PLHA(r, a, :) = mean(sqrt( (x1-x2).^2 + (y1-y2).^2 ));
    end
end

figure(99);
pairs = [1,2; 1,3; 1,4; 1,5; 2,3; 2,4; 2,5; 3,4; 3,5; 4,5];
for p = 1:10
    subplot(2,5,p);
    r = pairs(p, 1);
    a = pairs(p, 2);
    plot( squeeze(relative_pos_PL(r,a,:) - relative_pos_gt(r,a,:)) );
    hold on;
    plot( squeeze(relative_pos_PLH(r,a,:) - relative_pos_gt(r,a,:)) ); 
    plot( squeeze(relative_pos_PLA(r,a,:) - relative_pos_gt(r,a,:)) ); 
    plot( squeeze(relative_pos_PLHA(r,a,:) - relative_pos_gt(r,a,:)) );
    title(['avg distance ' num2str(r) ' and ' num2str(a)]);
    ylim([-0.4, 0.6]);
    
%     plot( squeeze(relative_pos_gt(r,a,:)) ); 
%     legend('PL', 'PLH', 'PLA', 'PLHA', 'GT')
    legend('PL', 'PLH', 'PLA', 'PLHA')
end

if saveplots
    saveas(gcf, 'figures_temp/fig91.fig'), saveas(gcf, 'figures_temp/fig91.png'), saveas(gcf, 'figures_temp/fig91','epsc');
end

end


%% Correlation/scatter between MSE and max blackout for each robot

% removing initial time instants, when the filter is still affected by the
% high initial error
corr_init = 100;
ind_obscamera_corr = ind_obscamera(corr_init:end);
time_vec_obscamera_corr = time_vec_obscamera(corr_init:end);

mse_dist_avg = squeeze(mean(mse_dist_time_total_plot, 1));
rmse_dist_avg_obscamera_corr = sqrt(mse_dist_avg(ind_obscamera_corr,:,:));

mse_psi_avg = squeeze(mean(mse_psi_time_total_plot, 1));
rmse_psi_avg_obscamera_corr = sqrt(mse_psi_avg(ind_obscamera_corr,:,:));

meanBlackoutLength = squeeze(mean(blackoutLengths,1));
meanBlackoutLength_corr = meanBlackoutLength(corr_init:end,:);
maxBlackoutLength_corr = maxBlackoutLength(:,corr_init:end);

if false
    figure(95);
    for r = 1:R
        subplot(2,R,r);
        plot(time_vec_obscamera, maxBlackoutLength(r,:));
        ylabel('maxBlackoutLength')

        subplot(2,R,r+R);
        plot(time_vec_obscamera, meanBlackoutLength(:,r));
        ylabel('meanBlackoutLength')
    end
end

if false
    for s = 1:4
        figure(95+s);
        for r = 1:R
            subplot(2,R,r);
    %         x = maxBlackoutLength_corr(r,:)';
            x = meanBlackoutLength_corr(:,r);
            y = rmse_dist_avg_obscamera_corr(:,r,s);
            scatter(x, y);
    %         coef = (x-mean(x))'*(y-mean(y))/(norm(x-mean(x))*norm(y-mean(y)));
            coef = corr(x(:), y(:), 'Type', 'Pearson');
    %         coef = corr(x(:), y(:), 'Type', 'Spearman');
            xlabel('blackout')
            ylabel('rmse dist');
            title(['R' num2str(r) ', ' setupNamePlot{s}, ', coef = ', num2str(coef)]);

            subplot(2,R,r+R);
    %         x = maxBlackoutLength_corr(r,:)';
            x = meanBlackoutLength_corr(:,r);
            y = rmse_psi_avg_obscamera_corr(:,r,s);
            scatter(x, y);
    %         coef = (x-mean(x))'*(y-mean(y))/(norm(x-mean(x))*norm(y-mean(y)));
            coef = corr(x(:), y(:), 'Type', 'Pearson');
    %         coef = corr(x(:), y(:), 'Type', 'Spearman');
            xlabel('blackout')
            ylabel('rmse psi');
            title(['R' num2str(r) ', ' setupNamePlot{s}, ', coef = ', num2str(coef)]);
        end
    end
end

figure(106);
for s = 1:4
    subplot(2,4,s);
    x = meanBlackoutLength_corr(:);
    y = rmse_dist_avg_obscamera_corr(:,:,s);
    y = y(:);
    scatter(x, y);
%     coef = (x-mean(x))'*(y-mean(y))/(norm(x-mean(x))*norm(y-mean(y)));
    coef = corr(x(:), y(:), 'Type', 'Pearson');
%     coef = corr(x(:), y(:), 'Type', 'Spearman');
    xlabel('mean blackout')
    ylabel('rmse dist')
    title(['all R, ', setupNamePlot{s} ', coef = ', num2str(coef)]);
    
    subplot(2,4,s+4);
    x = meanBlackoutLength_corr(:);
    y = rmse_psi_avg_obscamera_corr(:,:,s);
    y = y(:);
    scatter(x, y);
%     coef = (x-mean(x))'*(y-mean(y))/(norm(x-mean(x))*norm(y-mean(y)));
    coef = corr(x(:), y(:), 'Type', 'Pearson');
%     coef = corr(x(:), y(:), 'Type', 'Spearman');
    xlabel('mean blackout')
    ylabel('rmse psi')
    title(['all R, ', setupNamePlot{s} ', coef = ', num2str(coef)]);
end

figure(107);
for s = 1:4
    subplot(2,4,s);
    aux = maxBlackoutLength_corr.';
    x = aux(:);
    y = rmse_dist_avg_obscamera_corr(:,:,s);
    y = y(:);
    scatter(x, y);
%     coef = (x-mean(x))'*(y-mean(y))/(norm(x-mean(x))*norm(y-mean(y)));
    coef = corr(x(:), y(:), 'Type', 'Pearson');
%     coef = corr(x(:), y(:), 'Type', 'Spearman');
    xlabel('max blackout')
    ylabel('rmse dist')
    title(['all R, ', setupNamePlot{s} ', coef = ', num2str(coef)]);
    
    subplot(2,4,s+4);
    aux = maxBlackoutLength_corr.';
    x = aux(:);
    y = rmse_psi_avg_obscamera_corr(:,:,s);
    y = y(:);
    scatter(x, y);
%     coef = (x-mean(x))'*(y-mean(y))/(norm(x-mean(x))*norm(y-mean(y)));
    coef = corr(x(:), y(:), 'Type', 'Pearson');
%     coef = corr(x(:), y(:), 'Type', 'Spearman');
    xlabel('max blackout')
    ylabel('rmse psi')
    title(['all R, ', setupNamePlot{s} ', coef = ', num2str(coef)]);
end

figure(108);
obs_landmarks_corr = obs_landmarks_average(:,ind_obscamera_corr);
for s = 1:4
    subplot(2,4,s);
    aux = obs_landmarks_corr';
    x = aux(:);
    y = rmse_dist_avg_obscamera_corr(:,:,s);
    y = y(:);
    scatter(x, y);
%     coef = (x-mean(x))'*(y-mean(y))/(norm(x-mean(x))*norm(y-mean(y)));
    coef = corr(x(:), y(:), 'Type', 'Pearson');
%     coef = corr(x(:), y(:), 'Type', 'Spearman');
    xlabel('avg landmarks')
    ylabel('rmse dist')
    title(['all R, ', setupNamePlot{s} ', coef = ', num2str(coef)]);
    
    subplot(2,4,s+4);
    aux = obs_landmarks_corr';
    x = aux(:);
    y = rmse_psi_avg_obscamera_corr(:,:,s);
    y = y(:);
    scatter(x, y);
%     coef = (x-mean(x))'*(y-mean(y))/(norm(x-mean(x))*norm(y-mean(y)));
    coef = corr(x(:), y(:), 'Type', 'Pearson');
%     coef = corr(x(:), y(:), 'Type', 'Spearman');
    xlabel('avg landmarks')
    ylabel('rmse psi')
    title(['all R, ', setupNamePlot{s} ', coef = ', num2str(coef)]);
end

x = mean(meanBlackoutLength_corr, 2);
mse_dist_avg_M_R = squeeze(mean(mean(mse_dist_time_total_plot, 1),3));
rmse_dist_avg_M_R = sqrt(mse_dist_avg_M_R);
mse_psi_avg_M_R = squeeze(mean(mean(mse_psi_time_total_plot, 1),3));
rmse_psi_avg_M_R = sqrt(mse_psi_avg_M_R);

figure(101);
subplot(1,2,1);
plot(time_vec_obscamera_corr, x)
subplot(1,2,2);
for s = 1:4
    plot(time_vec, rmse_dist_avg_M_R(:,s)); hold on
end

for s = 1:4    
    figure(102);
    subplot(2,4,s);
    y = rmse_dist_avg_M_R(ind_obscamera_corr,s);
    scatter(x, y);
%     coef = (x-mean(x))'*(y-mean(y))/(norm(x-mean(x))*norm(y-mean(y)));
    coef = corr(x(:), y(:), 'Type', 'Pearson');
%     coef = corr(x(:), y(:), 'Type', 'Spearman');
    xlabel('avg blackout')
    ylabel('rmse avg M R dist')
    title(['avg M R, ', setupNamePlot{s} ', coef = ', num2str(coef)]);
        
    figure(103);
    subplot(2,4,s);
    plot(time_vec(ind_obscamera_corr), y);
    xlabel('time camera obs');
    ylabel('rmse dist');
    title(setupNamePlot{s})
    subplot(2,4,s+4);
    plot(time_vec(ind_obscamera_corr), x);
    xlabel('time camera obs');
    ylabel('blackout length');
    title(setupNamePlot{s})
    
    figure(102);
    subplot(2,4,s+4);
    y = rmse_psi_avg_M_R(ind_obscamera_corr,s);
    scatter(x, y);
%     coef = (x-mean(x))'*(y-mean(y))/(norm(x-mean(x))*norm(y-mean(y)));
    coef = corr(x(:), y(:), 'Type', 'Pearson');
%     coef = corr(x(:), y(:), 'Type', 'Spearman');
    xlabel('avg blackout')
    ylabel('rmse avg M R psi')
    title(['avg M R, ', setupNamePlot{s} ', coef = ', num2str(coef)]);
        
    figure(104);
    subplot(2,4,s);
    plot(time_vec(ind_obscamera_corr), y);
    xlabel('time camera obs');
    ylabel('rmse psi');
    title(setupNamePlot{s})
    subplot(2,4,s+4);
    plot(time_vec(ind_obscamera_corr), x);
    xlabel('time camera obs');
    ylabel('blackout length');
    title(setupNamePlot{s})
end


%% Plotting each robot with the four methods and all Monte Carlos
% for r = 1:R
%     figure(20+r); close
% end
% 
% for r = 1:R
%     figure(20+r);
%     for s = 1:4
%         aux = mse_dist{r,s};
%         for m = 1:size(aux,1)
%             if m <=10
%                 subplot(2,5, mod(m,10)+1 );
%                 plot(time_vec, aux(m,:));
%                 ylim([0, 1]);
%                 xlim([0, time_vec(end)]);            
%                 xlabel('Time (s)'); ylabel('Position MSE (m^2)');
%                 hold all
%             end
%         end
%     end
% end

%% Plotting each robot with the four methods and all Monte Carlos
% for r = 1:R
%     figure(30+r); close
% end
% 
% for r = 1:R
%     figure(30+r);
%     for s = 1:4
%         aux = mse_psi{r,s};
%         for m = 1:size(aux,1)
%             if m < 10
%                 subplot(2,5,m);
%                 plot(time_vec, aux(m,:));
%                 ylim(ylim_psi_param);
%                 xlim([0, time_vec(end)]);            
%                 xlabel('Time (s)'); ylabel('Orientation MSE (rad^2)');
%                 hold all
%             end
%         end
%     end
% end

%% Plotting standard deviation
% % figure(40);
% 
% saveplots = true;
% 
% ylim_dist = [-0.5, 1.5;
%              -1.5, 3.5;
%              -3.0, 5.5;
%              -0.5, 1.5;
%              -0.5, 1.5];
%          
% 
% ylim_psi  = [-0.10, 0.15;
%              -0.20, 0.30;
%              -0.20, 0.30;
%              -0.10, 0.15;
%              -0.10, 0.15];
% 
% for s = [1,4]
%     for r = 1:R
%         
%         aux = mse_dist{r,s};
%         mu = mean(aux,1);
%         sigma = std(aux,[],1);
%         
%         figure;
%         plot(time_vec, mu);        
%         hold on;
%         errorbar(time_vec(45:200:end), mu(45:200:end), sigma(45:200:end), 'o');
%         hold off;
%         xlabel('Time (s)'); ylabel('Position RMSE (m)'); 
%         title(['Robot ' num2str(r) ' method: ' setupNamePlot{s} ]);
%         ylim(ylim_dist(r,:));
%         
%         foldername = 'figures_temp/';
%         figname = ['robot' num2str(r) '_pos_' setupNamePlot{s} ];
%         
%         if saveplots
%             saveas(gcf, [foldername figname '.fig'])
%             saveas(gcf, [foldername figname '.png'])
%             saveas(gcf, [foldername figname], 'epsc')
% %             saveas(gcf, 'figures_temp/fig6.fig')
% %             saveas(gcf, 'figures_temp/fig6.png')
% %             saveas(gcf, 'figures_temp/fig6','epsc')
%         end
%                 
%         
%         aux = mse_psi{r,s};
%         mu = mean(aux,1);
%         sigma = std(aux,[],1);
%         
%         figure;
%         plot(time_vec, mu);        
%         hold on;
%         errorbar(time_vec(45:200:end), mu(45:200:end), sigma(45:200:end), 'o');
%         hold off;
%         xlabel('Time (s)'); ylabel('Orientation RMSE (rad)'); 
%         title(['Robot ' num2str(r) ' method: ' setupNamePlot{s}]);
%         ylim(ylim_psi(r,:));
%         
%         foldername = 'figures_temp/';
%         figname = ['robot' num2str(r) '_psi_' setupNamePlot{s} ];
%         
%         if saveplots
%             saveas(gcf, [foldername figname '.fig'])
%             saveas(gcf, [foldername figname '.png'])
%             saveas(gcf, [foldername figname], 'epsc')
% %             saveas(gcf, 'figures_temp/fig6.fig')
% %             saveas(gcf, 'figures_temp/fig6.png')
% %             saveas(gcf, 'figures_temp/fig6','epsc')
%         end
%         
%     end
% end
% 
