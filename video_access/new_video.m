clear all; close all

addpath('../plotresults_functions', '../drawfield_functions')
% load('output_final/sim41/sim41.mat'); % finais paper, Np = 100
load('D:\OneDrive\sim107.mat');

%%
landmarks = data.simConfig.landmarks;
T = data.simConfig.T;
R = data.simConfig.R;
npoints = 1000;

%% video1: ground truth, all robots
video1 = VideoWriter('review_GT.avi');
video1.FrameRate = 1/T;
open(video1);

%% video2: PL and PLHA vs. GT, robot 2, monte carlo 1
video2 = VideoWriter('review_PLvsPLHA_robot2.avi');
video2.FrameRate = 1/T;
open(video2);

%% video3: PL and PLHA vs. GT, robot 2, monte carlo 1
video3 = VideoWriter('review_PLvsPLHA_robot2_zoom.avi');
video3.FrameRate = 1/T;
open(video3);


%% gerar trajetória verdadeira
m = 1; % select particular monte carlo realization
gt = cell(R,1);
est_PL = cell(R,1);
est_PLHA = cell(R,1);
for r = 1:R
    gt{r} = data.TeamEstSave{r,1}.realData(1:npoints, :); % npoints x 3;

    % gerar posições estimadas pelo PL
    err_x_PL = reshape(err_x_time_total(m, 1:npoints, r, 1), [npoints,1]); % npoints x 1
    err_y_PL = reshape(err_y_time_total(m, 1:npoints, r,1), [npoints,1]); % npoints x 1
    err_psi_PL = reshape(err_psi_time_total(m, 1:npoints, r,1), [npoints,1]); % npoints x 1

    est_PL{r} = gt{r} + [err_x_PL, err_y_PL, err_psi_PL];

    % gerar posições estimadas pelo PLHA
    err_x_PLHA = reshape(err_x_time_total(m, 1:npoints,r,3), [npoints,1]); % npoints x 1
    err_y_PLHA = reshape(err_y_time_total(m, 1:npoints,r,3), [npoints,1]); % npoints x 1
    err_psi_PLHA = reshape(err_psi_time_total(m, 1:npoints,r,3), [npoints,1]); % npoints x 1

    est_PLHA{r} = gt{r} + [err_x_PLHA, err_y_PLHA, err_psi_PLHA];
end

%% video 1: ground truth
figure(1);
for n = 1:npoints
    hold off;
    drawField(landmarks);
    ylim([-11, +11]);
    d = 780;
    set(gcf, 'position', [100,10, floor(1.4*d),d]);
    
    hold on;
    for r = 1:R
        drawPlayer( gt{r}(n,1), gt{r}(n,2), gt{r}(n,3), [], r );
        plot( gt{r}(1:n,1),gt{r}(1:n,2), 'k' );
    end
    daspect([1 1 1]);
    
%     pause(T)
    frame1 = getframe(gcf);
    writeVideo(video1, frame1);
end
close(video1);

%% video 2: robot 2, one MC, GT and PL
figure(2);
for n = 1:npoints
    hold off;
    drawField(landmarks);
    ylim([-11, +11]);
    d = 780;
    set(gcf, 'position', [100,10, floor(1.4*d),d]);
    hold on;
    
    r = 2;
    drawPlayer( gt{r}(n,1), gt{r}(n,2), gt{r}(n,3), data.TeamEstSave{2,4}.visionAngle, r );
    drawPlayer( est_PL{r}(n,1), est_PL{r}(n,2), est_PL{r}(n,3), [], r, [0 0.4470 0.7410] );
    drawPlayer( est_PLHA{r}(n,1), est_PLHA{r}(n,2), est_PLHA{r}(n,3), [], r, [0.4940 0.1840 0.5560] );
    
    % plotar trajetórias
    gt_traj = plot( gt{r}(1:npoints,1),gt{r}(1:npoints,2), 'Color', [0, 0, 0] );
%     plot( gt{r}(1:n,1),gt{r}(1:n,2), 'Color', [0, 0, 0] );
    PL_traj = plot( est_PL{r}(1:n,1), est_PL{r}(1:n,2), 'Color', [0 0.4470 0.7410] );
    PLHA_traj = plot( est_PLHA{r}(1:n,1), est_PLHA{r}(1:n,2), 'Color', [0.4940 0.1840 0.5560] );
    daspect([1 1 1]);
    
    legend( [gt_traj, PL_traj, PLHA_traj], {'Ground truth', 'PL estimate', 'PLHA estimate'} );
                
    frame2 = getframe(gcf);
    writeVideo(video2, frame2);
end
close(video2);

%% video 3: robot 2, one MC, GT and PL. Zoomed in
figure(3);
for n = 1:npoints
    hold off;
    drawField(landmarks);
    xlim([-6, +3]);
    ylim([-3, +3]);
    d = 780;
    set(gcf, 'position', [100,10, floor(1.4*d),d]);
    hold on;
    
    r = 2;
    drawPlayer( gt{r}(n,1), gt{r}(n,2), gt{r}(n,3), [], r );
    drawPlayer( est_PL{r}(n,1), est_PL{r}(n,2), est_PL{r}(n,3), [], r, [0 0.4470 0.7410] );
    drawPlayer( est_PLHA{r}(n,1), est_PLHA{r}(n,2), est_PLHA{r}(n,3), [], r, [0.4940 0.1840 0.5560] );
    
    % plotar trajetórias
    gt_traj = plot( gt{r}(1:npoints,1),gt{r}(1:npoints,2), 'Color', [0, 0, 0] );
    PL_traj = plot( est_PL{r}(1:n,1), est_PL{r}(1:n,2), 'Color', [0 0.4470 0.7410] );
    PLHA_traj = plot( est_PLHA{r}(1:n,1), est_PLHA{r}(1:n,2), 'Color', [0.4940 0.1840 0.5560] );
    daspect([1 1 1]);
    
    legend( [gt_traj, PL_traj, PLHA_traj], {'Ground truth', 'PL estimate', 'PLHA estimate'} );
    
%     pause(T)            
    frame = getframe(gcf);
    writeVideo(video3, frame);
end
close(video3);

