function [mse_dist_no_outlier, mse_psi_no_outlier, outlierList] = removeOutliers(mse_dist_time_total, mse_psi_time_total, err_x_time_total, err_y_time_total, err_psi_time_total, k, R, M, nIterations)

    % Applying modified z-test
    % individually on x and y axes; if a monte carlo is an outlier in either one of
    % them, the sample is an outlier
    % outlier detection is performed using modified z-test: assumes the
    % distribution of errors is Gaussian. Then, calculates the median and the
    % MAD: median of the absolute deviations from the median, as a way to
    % remove outliers without them affecting the sample mean and variance.

    mse_dist_time_sub = mse_dist_time_total(:, :, :, :);
    mse_psi_time_sub = mse_psi_time_total(:, :, :, :);
    
    err_x_time_sub = err_x_time_total(:, :, :, :);
    err_y_time_sub = err_y_time_total(:, :, :, :);
    err_psi_time_sub = err_psi_time_total(:, :, :, :);

    % applying modified z test for each time, robot, method (i, r, s)
    % outliers are identified in x and y individually (gaussians)
    % error quantification is done on mse_dist
    outlierList = cell(R,4);
    mse_dist_no_outlier = cell(R,4);

    % para N(0,2^2), sigma = 2 e MAD = 0.66*sigma
    % logo, se os outliers est�o em k*sigma, o threshold usando mad deve ser
    % 1.5*k*mad
    
    k = 5.5*1000;
    threshold = 1.5*k;
    for r = 1:R
        for s = 1:4
            list = [];
            for i = 1:nIterations
                err_x_allmc = err_x_time_sub(:,i,r,s);
                med_x = median(err_x_allmc);
                mad_x = median( abs(err_x_allmc - med_x) );

                err_y_allmc = err_y_time_sub(:,i,r,s);
                med_y = median(err_y_allmc);
                mad_y = median( abs(err_y_allmc - med_y) );
                for m = 1:M
                    z_value_x = (err_x_allmc(m) - med_x)/mad_x;
                    z_value_y = (err_y_allmc(m) - med_y)/mad_y;
                    if abs(z_value_x) > threshold || abs(z_value_y) > threshold           
                        list = [list, m];  
                    end
                end            
            end
            list = sort(unique(list)); %sort and remove duplicate values from list
            aux = mse_dist_time_sub(:,:,r,s);
            aux(list,:) = []; % even if list has repeated values, only one row is removed
            mse_dist_no_outlier{r,s} = aux;
            outlierList{r,s} = list;
        end
    end
    
    % plot how many outliers have been removed on each method/robot
    figure(17);
    for r=1:R
        outliers_per_method = zeros(4,1);
        for s=1:4
            outliers_per_method(s) = length(outlierList{r,s});
    %         subplot(5,4,s+(r-1)*4)
    %         stem(outlierList{r,s}, ones(length(outlierList{r,s})))
    %         size(outlierList{r,s})
        end
        subplot(1,5,r)
        plot([1:4], outliers_per_method, 'rx:')
        xlim([0,5]); ylim([0,100])
        xlabel('method'); ylabel('# outliers removed'); title(['robot ' num2str(r)]);
    end
    
    % Removing outliers in Psi (will not be done soon)
    mse_psi_no_outlier = cell(R,4);
    for r=1:R
        for s = 1:4
            mse_psi_no_outlier{r,s} = mse_psi_time_sub(:,:,r,s);
        end
    end
    
    
end