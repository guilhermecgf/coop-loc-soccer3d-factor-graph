function [ streakCount ] = countBlackout( numberObs )

    len = length(numberObs);
    streakCount = zeros( size(numberObs) );
    
%     for i = len:-1:1        
%         if i == len
%             if numberObs(i) == 0
%                 streakCount(i) = 1;
%             else
%                 streakCount(i) = 0;
%             end
%         else
%             if numberObs(i) == 0
%                 streakCount(i) = 1 + streakCount(i+1);
%             else
%                 streakCount(i) = 0;
%             end
%         end
%     end
    
    for i = 1:len        
        if i == 1
            if numberObs(i) == 0
                streakCount(i) = 1;
            else
                streakCount(i) = 0;
            end
        else
            if numberObs(i) == 0
                streakCount(i) = 1 + streakCount(i-1);
            else
                streakCount(i) = 0;
            end
        end
    end
end

