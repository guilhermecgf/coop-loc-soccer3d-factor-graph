% Returns which points of pointList are seen, in global coordinates, along
% with the label of each of the landmarks on its set.
% Labels can be used for features which can be considered perfectly
% identified when observed, such as corners with different colors
function [viewedPoints, labels] = visionPoint(x,y,psi,visionAngle,pointList)
angle_min = psi - visionAngle;
angle_max = angle_min + 2*pi;
resultIndex = 1;
nothingFound = true;
for index = 1:length(pointList(:,1))
    
    % special case: the observed point is exactly on the current position
    % in this case, it will never be observed
    if ~( pointList(index,2) == y && pointList(index,1) == x )
    
        angle(index) = atan2(pointList(index,2)-y,pointList(index,1)-x);
        %convert the angle to the desired interval
        while( angle(index) < angle_min)
            angle(index) = angle(index) + 2*pi;
        end
        while(angle(index) > angle_max)
            angle(index) = angle(index) - 2*pi;
        end
        
        if angle(index) < (angle_min+2*visionAngle)
            viewedPoints(resultIndex,:) = pointList(index,:);
            labels(resultIndex) = index;
            resultIndex = resultIndex + 1;
            nothingFound = false;
        end
        
    end
end
if nothingFound
    viewedPoints = 0;
    labels = [];
end
