function [distance, bearing] = cartesianToDistanceBearing(x, y)
    distance = sqrt(x*x + y*y);
    bearing = atan2(y, x);
end