function pointLocal = globalToLocalPoint(x,y,psi,pointGlobal)

    pointLocal = zeros(1,2);
    pointLocal(1,1) = (pointGlobal(1,1)-x)*cos(psi) + (pointGlobal(1,2)-y)*sin(psi);
    pointLocal(1,2) = -(pointGlobal(1,1)-x)*sin(psi) + (pointGlobal(1,2)-y)*cos(psi);

end
