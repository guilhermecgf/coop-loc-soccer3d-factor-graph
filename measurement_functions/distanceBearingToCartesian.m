function [x, y] = distanceBearingToCartesian(distance, bearing)
    x = distance * cos(bearing);
    y = distance * sin(bearing);
end