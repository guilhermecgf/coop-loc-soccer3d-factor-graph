function viewdLines = visionLine(x,y,psi,visionAngle,lineList,config)

ang_min = psi-visionAngle;
ang_max = psi+visionAngle;


void = 1; %flag to indicate there are no observed lines (might not be necessary as there should always be lines wherever the robot looks)
resultIndex=1;


for i = 1:length(lineList(:,1))
    
    % matrixes for line intersection equations
    a1 = [-tan(ang_min) 1; lineList(i,2)-lineList(i,4) -lineList(i,1)+lineList(i,3)];
    a2 = [-tan(ang_max) 1; lineList(i,2)-lineList(i,4) -lineList(i,1)+lineList(i,3)];
    b1 = [y-x*tan(ang_min); lineList(i,3)*lineList(i,2)-lineList(i,1)*lineList(i,4)];
    b2 = [y-x*tan(ang_max); lineList(i,3)*lineList(i,2)-lineList(i,1)*lineList(i,4)];
    
    extreme1 = [lineList(i,1) lineList(i,2)];
    extreme2 = [lineList(i,3) lineList(i,4)];
    
    % if at least one the line's extremes is observed, the line itself is observed
    if  extreme1 == visionPoint(x,y,psi,visionAngle,extreme1) | extreme2 == visionPoint(x,y,psi,visionAngle,extreme2)
        void = 0;
        dist = abs(a1(2,1)*x+a1(2,2)*y-b1(2))/sqrt(a1(2,1)^2 + a1(2,2)^2);
        bear = bearing(x,y,dist,lineList(i,1),lineList(i,2),lineList(i,3),lineList(i,4),psi);
        viewdLines(resultIndex,1) = mvnrnd(bear,config.bearingVariance);
        viewdLines(resultIndex,2) = mvnrnd(dist,config.distanceLineVariance);
        resultIndex = resultIndex + 1;
        
        % if the extremes are not observed and there are intersections, the intersections' midpoint must be observed
        % the midpoint must also be in the interval between the extremes
    elseif det(a1)~=0 & det(a2)~=0
        r = (linsolve(a1,b1) + linsolve(a2,b2))/2;
        if ( r(1)<=lineList(i,1) & r(1)>=lineList(i,3) ) | ( r(1)>=lineList(i,1) & r(1)<=lineList(i,3) )
            if ( r(2)<=lineList(i,2) & r(2)>=lineList(i,4) ) | ( r(2)>=lineList(i,2) & r(2)<=lineList(i,4) )
                if r' == visionPoint(x,y,psi,visionAngle,r')
                    void = 0;
                    dist = abs(a1(2,1)*x+a1(2,2)*y-b1(2))/sqrt(a1(2,1)^2 + a1(2,2)^2);
                    bear = bearing(x,y,dist,lineList(i,1),lineList(i,2),lineList(i,3),lineList(i,4),psi);
                    viewdLines(resultIndex,1) = mvnrnd(bear,config.bearingVariance) ;
                    viewdLines(resultIndex,2) = mvnrnd(dist,config.distanceLineVariance);
                    resultIndex = resultIndex + 1;
                end
            end
        end
    end
    
    
end

if void
    viewdLines = 0;
end

end

