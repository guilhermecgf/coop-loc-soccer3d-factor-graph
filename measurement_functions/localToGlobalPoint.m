function pointGlobal = localToGlobalPoint(x,y,psi,pointLocal)


pointGlobal = zeros(1,2);
pointGlobal(1,1) = (pointLocal(1,1))*cos(psi) - (pointLocal(1,2))*sin(psi) + x;
pointGlobal(1,2) = (pointLocal(1,1))*sin(psi) + (pointLocal(1,2))*cos(psi) + y;


end