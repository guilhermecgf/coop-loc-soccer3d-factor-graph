% Determines which features (such as landmarks or other players) a robot
% can see, given its position, pose and angle of vision.
% The plotting step is also performed here
% Returns observations in Global and Local (defined by x,y,psi) reference
% frames, without requiring further reference conversion

function [observedGlobal, measurement] = observation(x,y,psi,visionAngle,landmarks,currentPlayerPos,config,M)
% function [observedGlobal, measurement] = observation(x,y,psi,visionAngle,landmarks,currentPlayerPos,config)

    % This function can be further modularized

    % players, goal posts and field corners are returned with labels, as we
    % choose to represent them as uniquely tagged.
    observedGlobal.players = [];
    observedGlobal.playersID = [];
    observedGlobal.goals = [];
    observedGlobal.goalsID = [];
    observedGlobal.corners = [];
    observedGlobal.cornersID = [];
    
    measurement.players = [];
    measurement.playersID = [];
    measurement.goals = [];
    measurement.goalsID = [];
    measurement.corners = [];
    measurement.cornersID = [];
    
    %disp('observing players');

    [observedGlobal_noisefree, observedGlobal.playersID] = visionPoint(x,y,psi,visionAngle,currentPlayerPos);
    % up to this point, observedGlobal has no noise form observation, which
    % is currently added in the local reference frame

    if ~isscalar(observedGlobal_noisefree)
        for i = 1:length(observedGlobal_noisefree(:,1))
                        
            state_self = [x; y; psi];
            state_target = [observedGlobal_noisefree(i,:).'; 0];
            range = hRho(state_self, state_target);
            bearing = hPhi(state_self, state_target);
            
%             % WITHOUT MULTIPLE MEASUREMENTS, FOR MONTE CARLO
%             measurement.players(i,:) = [range + sqrt(config.rangeVariancePar*range/100)*randn, bearing + sqrt(config.bearingVariance)*randn];
%             measurement.playersID = observedGlobal.playersID;
            
            % WITH MULTIPLE MEASUREMENTS, FOR MONTE CARLO
            %%% TODO: range e /100: tirar daqui, trocar por range^2
            range_array = range + sqrt(config.rangeVariancePar*range^2)*randn(1, M);
            bearing_array = bearing + sqrt(config.bearingVariance)*randn(1, M);
            bearing_array = wrapAngle(bearing_array);
            
            
            % measurement.players(a,:,m): ally a, complete vector of
            % measurement, monte carlo realization m
            measurement.players(i,:,:) = [range_array; bearing_array];
            measurement.playersID = observedGlobal.playersID;
            
        end
        for i = 1:length(measurement.players(:,1))
            % reconversion to corresponding global position after noise has been added;
            observedGlobal.players(i,1) = x + measurement.players(i,1)*cos( psi + measurement.players(i,2) );
            observedGlobal.players(i,2) = y + measurement.players(i,1)*sin( psi + measurement.players(i,2) );
        end
    end

    %disp('observing goals');
    
    [observedGlobal_noisefree, observedGlobal.goalsID] = visionPoint(x,y,psi,visionAngle,landmarks.goalList);

    if ~isscalar(observedGlobal_noisefree)
        for i = 1:length(observedGlobal_noisefree(:,1))
          
            state_self = [x; y; psi];
            state_target = [observedGlobal_noisefree(i,:).'; 0];
            range = hRho(state_self, state_target);
            bearing = hPhi(state_self, state_target);
            
%             % WITHOUT MULTIPLE MEASUREMENTS, FOR MONTE CARLO
%             measurement.goals(i,:) = [range + sqrt(config.rangeVariancePar*range/100)*randn, bearing + sqrt(config.bearingVariance)*randn];
%             measurement.goalsID = observedGlobal.goalsID;
            
            % WITH MULTIPLE MEASUREMENTS, FOR MONTE CARLO
            range_array = range + sqrt(config.rangeVariancePar*range^2)*randn(1, M);
            bearing_array = bearing + sqrt(config.bearingVariance)*randn(1, M);
            bearing_array = wrapAngle(bearing_array);
            
            % measurement.goals(g,:,m): goal g, complete vector of
            % measurement, monte carlo realization m
            measurement.goals(i,:,:) = [range_array; bearing_array];
            measurement.goalsID = observedGlobal.goalsID;
            
        end
        for i = 1:length(measurement.goals(:,1))
            % reconversion to corresponding global position after noise has been added;
            observedGlobal.goals(i,1) = x + measurement.goals(i,1)*cos( psi + measurement.goals(i,2) );
            observedGlobal.goals(i,2) = y + measurement.goals(i,1)*sin( psi + measurement.goals(i,2) );
        end
    end

    %disp('observing corners');
    
    [observedGlobal_noisefree, observedGlobal.cornersID] = visionPoint(x,y,psi,visionAngle,landmarks.cornerList);

    if ~isscalar(observedGlobal_noisefree)
        for i = 1:length(observedGlobal_noisefree(:,1))

            state_self = [x; y; psi];
            state_target = [observedGlobal_noisefree(i,:).'; 0];
            range = hRho(state_self, state_target);
            bearing = hPhi(state_self, state_target);
            
%             % WITHOUT MULTIPLE MEASUREMENTS FOR MONTE CARLO
%             measurement.corners(i,:) = [range + sqrt(config.rangeVariancePar*range/100)*randn, bearing + sqrt(config.bearingVariance)*randn];
%             measurement.cornersID = observedGlobal.cornersID;
            
            % WITH MULTIPLE MEASUREMENTS FOR MONTE CARLO
            range_array = range + sqrt(config.rangeVariancePar*range^2)*randn(1, M);
            bearing_array = bearing + sqrt(config.bearingVariance)*randn(1, M);
            bearing_array = wrapAngle(bearing_array);
            
            % measurement.corners(c,:,m): corner c, complete vector of
            % measurement, monte carlo realization m
            measurement.corners(i,:,:) = [range_array; bearing_array];
            measurement.cornersID = observedGlobal.cornersID;

        end
        for i = 1:length(measurement.corners(:,1))
            % reconversion to corresponding global position after noise has been added;
            observedGlobal.corners(i,1) = x + measurement.corners(i,1)*cos( psi + measurement.corners(i,2) );
            observedGlobal.corners(i,2) = y + measurement.corners(i,1)*sin( psi + measurement.corners(i,2) );
        end
    end

end
