% %% Script to plot the plots desired in the paper.
% Position and orientation error, bars per method, objects detected
clear all; close all
addpath('plotresults_functions')
% load('output_final/2020022-novomc/novomc-2627fev.mat');
% load('output_final/sim29/sim29.mat');
% load('output_final/sim31/sim31.mat');
% load('output_final/sim37/sim37.mat');
% load('output_final/sim41/sim41.mat'); % finais paper, Np = 100
% load('output_final/sim67.mat'); % finais paper, Np = 200, most recent belief = false
% load('output_final/sim71.mat'); % finais paper, Np = 200, most recent belief = true, new trajectory for robot 2
% load('D:\OneDrive\sim71.mat'); % finais paper, Np = 200, most recent belief = true
% load('D:\OneDrive\sim73.mat'); % finais paper, Np = 300, most recent belief = false
% load('D:\OneDrive\sim102.mat'); % finais paper, Np = 200, most recent belief = false. Seeds fixas
load('D:\OneDrive\sim107.mat'); % finais paper, Np = 200, most recent belief = false. Seeds fixas, MC 130

% load('output_final/sim493.mat'); % one sample MPF, Np = 100
% load('output_final/sim983.mat'); % one sample MPF, Np = 400
% load('output_final/sim173.mat'); % one sample MPF, Np = 1000
% load('output_final/sim867.mat'); % one sample MPF, mean, Np = 400
% load('output_final/sim958.mat'); % one sample MPF, Np = 400, T = 0.02
% load('output_final/sim495.mat'); % only P, Np = 100
% load('output_final/sim284.mat'); % one sample MPF, mean, Np = 400

saveplots = false;

obs_landmarks_average = squeeze(mean(obs_landmarks_total,1));
obs_players_average = squeeze(mean(obs_players_total,1));

%% Convert to cell format
mse_dist = cell(R,4);
mse_psi = cell(R,4);

for r = 1:R
    for s = 1:4
        mse_dist{r,s} = mse_dist_time_total(:,:,r,s);
        mse_psi{r,s} = mse_psi_time_total(:,:,r,s);
    end
end

%% If desired, remove outliers
boolRemoveOutliers = false;
if boolRemoveOutliers
    % k is number of standard deviations from the mean; internally,
    % function treats with median and absolute error
    k = 10;
    [mse_dist, mse_psi] = removeOutliers(mse_dist_time_total, mse_psi_time_total, err_x_time_total, err_y_time_total, err_psi_time_total, k, R, M, nIterations);
end

%% Another way of removing outliers: considering only MSE averaged over all time instants
boolRemoveOutliers = false;
if boolRemoveOutliers
    k = 4;
    [mse_dist, mse_psi, ind_no_outliers_dist, ind_no_outliers_psi] = removeOutliersNew(mse_dist, mse_psi, k);
    ind_no_outliers_dist, ind_no_outliers_psi
end

%%
% Para plotar, vamos deixar o plot do PLHA por �ltimo.
setupNamePlot = {'PL', 'PLH', 'PLA', 'PLHA'};
for r = 1:R
    aux = mse_dist{r,3};
    mse_dist{r,3} = mse_dist{r,4};
    mse_dist{r,4} = aux;
end

for r = 1:R
    aux = mse_psi{r,3};
    mse_psi{r,3} = mse_psi{r,4};
    mse_psi{r,4} = aux;
end

%% Plots finais para o artigo: distancia
time_vec = T * [1:nIterations];
% sub_time = 2:(nIterations/2);
sub_time = 1:nIterations;

xlim_param = [0, time_vec(end)];
ylim_dist_param = [0, 1.6];

% PLOT 1: en vs. tempo (erro medio, entre todos os robos, versus tempo),
% para cada metodo

% conteudo esta em mse_dist{r,s}, que e uma matriz M vs. nIteration
en_dist_metodo = zeros(4, nIterations);
enr_dist_metodo = zeros(R, nIterations, 4);
for s=1:4
    enr_dist = zeros(R, nIterations);
    for r=1:R
        
        aux = mse_dist{r,s};
        mse_dist{r,s} = aux;
        
        % mean over M        
        enr_dist(r,:) = mean(mse_dist{r,s}, 1);
    end
    % mean over R
    enr_dist_metodo(:,:,s) = enr_dist;
    en_dist_metodo(s,:) = mean(enr_dist,1);
end

% reminder: what must be plotted is rmse
figure(1);
linetype = {'x-', 'v-', 'o-', 's-'};
linetype = {':', ':', ':', ':'};
for s=1:4
    plot(time_vec(sub_time), sqrt(en_dist_metodo(s,sub_time)), linetype{s}, 'LineWidth', 1.5); hold all;
%     semilogy(time_vec(sub_time), sqrt(en_dist_metodo(s,sub_time)), linetype{s}, 'LineWidth', 1.5); hold all;
end
grid on; hold off;
legend(setupNamePlot);
xlabel('Time (s)'); ylabel('Position RMSE (m)'); 
% title('Position error, averaged over all robots');
xlim(xlim_param); ylim(ylim_dist_param)

if saveplots
    saveas(gcf, 'figures_temp/res_fig1.fig'), saveas(gcf, 'figures_temp/res_fig1.png'), saveas(gcf, 'figures_temp/res_fig1','epsc');
end

% PLOT 2: er para cada m�todo. 5 barras por m�todo
er_dist_metodo = zeros(4, R);
for s = 1:4
    for r = 1:R
        % mean over Monte Carlo and time
        mean_mc = mean(mse_dist{r,s}, 1);
        mean_mc_time = mean(mean_mc(sub_time));
%         er_dist_metodo(s,r) = mean(mean(mse_dist{r,s}));
        er_dist_metodo(s,r) = mean_mc_time;
    end
end

figure(2);
bar(sqrt(er_dist_metodo)); ylim(ylim_dist_param);
ylabel('Position RMSE (m)'); xlabel('Variant')
legend('Robot 1', 'Robot 2', 'Robot 3', 'Robot 4', 'Robot 5');
% title('Position error, for each robot, averaged over all time instants.');
set(gca, 'XTickLabelMode', 'manual');
set(gca, 'XTickLabel', setupNamePlot);

if saveplots
    saveas(gcf, 'figures_temp/res_fig2.fig'), saveas(gcf, 'figures_temp/res_fig2.png'), saveas(gcf, 'figures_temp/res_fig2','epsc');
end

%% Plots finais para o artigo: orientation

% PLOT 3: en vs. tempo (erro m�dio, entre todos os rob�s, versus tempo),
% para cada m�todo

% conte�do est� em mse_psi{r,s}, que � uma matriz M vs.
% nIteration
ylim_psi_param = [0, 0.6];

en_psi_metodo = zeros(4, nIterations);
enr_psi_metodo = zeros(R, nIterations, 4);
for s=1:4
    enr_psi = zeros(R, nIterations);
    for r=1:R
        % mean over M
        enr_psi(r,:) = mean(mse_psi{r,s}, 1);
    end
    % mean over R
    enr_psi_metodo(:,:,s) = enr_psi;
    en_psi_metodo(s,:) = mean(enr_psi,1);
end

% reminder: what must be plotted is rmse
figure(3);
linetype = {'x-', 'v-', 'o-', 's-'};
linetype = {':', ':', ':', ':'};
for s=1:4
    plot(time_vec(sub_time), sqrt(en_psi_metodo(s,sub_time)), linetype{s}, 'LineWidth', 1.5); hold all;
%     semilogy(time_vec(sub_time), sqrt(en_psi_metodo(s,sub_time)), linetype{s}, 'LineWidth', 1.5); hold all;
end
hold off; grid on;
legend(setupNamePlot);
xlabel('Time (s)'); ylabel('Orientation RMSE (rad)');
xlim(xlim_param); ylim(ylim_psi_param);

if saveplots
    saveas(gcf, 'figures_temp/res_fig3.fig'), saveas(gcf, 'figures_temp/res_fig3.png'), saveas(gcf, 'figures_temp/res_fig3','epsc');
end

% PLOT 4: er para cada m�todo. 5 barras por m�todo
er_psi_metodo = zeros(4, R);
for s = 1:4
    for r = 1:R
        % mean over Monte Carlo and time
        mean_mc = mean(mse_psi{r,s}, 1);
        mean_mc_time = mean(mean_mc(sub_time));
%         er_psi_metodo(s,r) = mean(mean(mse_psi{r,s}));
        er_psi_metodo(s,r) = mean_mc_time;
    end
end

figure(4); 
bar(sqrt(er_psi_metodo)); ylim(ylim_psi_param);
ylabel('Orientation RMSE (rad)'); xlabel('Variant')
legend('Robot 1', 'Robot 2', 'Robot 3', 'Robot 4', 'Robot 5');
set(gca, 'XTickLabelMode', 'manual');
set(gca, 'XTickLabel', setupNamePlot);

if saveplots
    saveas(gcf, 'figures_temp/res_fig4.fig'), saveas(gcf, 'figures_temp/res_fig4.png'), saveas(gcf, 'figures_temp/res_fig4','epsc');
end

%% Investigating worst and best robots

% number of observed landmarks on all monte carlo runs
count_observed_landmarks = zeros(5, nIterations, R);
for r = 1:R
    for i = 1:nIterations
        for o = 0:4
            count_observed_landmarks(o+1, i, r) = sum(obs_landmarks_total(:,r,i) == o);
        end
    end
end


% counting the maximum blackout length for each time instant, considering
% all Monte Carlo runs
ind_obscamera = 3:3:length(time_vec);
time_vec_obscamera = time_vec(ind_obscamera);
maxBlackoutLength = zeros(R, length(time_vec_obscamera));
blackoutLengths = zeros(M, length(time_vec_obscamera), R);
for r = 1:R
    for m = 1:M
        aux = countBlackout(squeeze(obs_landmarks_total(m,r,ind_obscamera)));
        blackoutLengths(m,:,r) = reshape(aux, [1, numel(aux)]); 
    end
    aux2 = blackoutLengths(:,:,r);
    aux3 = max(aux2,[], 1);
    maxBlackoutLength(r, :) = aux3;
end


%% Buscando uma maneira de encaixar todos os gr�ficos conjuntamente

% PLOT 5: casos mais favor�veis para coopera��o: rob�s que veem poucos landmarks (2 e 3)
figure(5);
r1 = 2;
s_vec = [1,4];
% subplot(18,1,1:6);
subplot(24,1,1:6);
for s = s_vec
    mean_mse = mean( mse_dist{r1,s}, 1);
    var_mse = var( mse_dist{r1,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
    plot(time_vec(sub_time), sqrt(mean_mse(sub_time)), ':', 'LineWidth', 1.5);
    hold on;
end
grid on;
legend(['Robot ' num2str(r1) ' - ' setupNamePlot{1}], ['Robot ' num2str(r1) ' - ' setupNamePlot{4}], 'Location', 'northeast');
% xlabel('Time (s)'); 
ylabel('Position RMSE (m)');
xlabel('(a)');
xlim(xlim_param); ylim(ylim_dist_param);

% subplot(18,1,8:13);
subplot(24,1,9:14);
for s = s_vec
    mean_mse = mean( mse_psi{r1,s}, 1);
    var_mse = var( mse_psi{r1,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
    plot(time_vec(sub_time), sqrt(mean_mse(sub_time)), ':', 'LineWidth', 1.5);
%     semilogy(time_vec(sub_time), sqrt(mean_mse(sub_time)), [linetype{r1}], 'LineWidth', 1.5);
    hold on;
end
grid on;
legend(['Robot ' num2str(r1) ' - ' setupNamePlot{1}], ['Robot ' num2str(r1) ' - ' setupNamePlot{4}], 'Location', 'northeast');
% xlabel('Time (s)'); 
ylabel('Orientation RMSE (rad)');
xlabel('(b)');
xlim(xlim_param); ylim(ylim_psi_param);

% subplot(18,1,15:18);
subplot(24,1,17:19);
total_camera_obs = [obs_landmarks_average(r1,sub_time); obs_players_average(r1,sub_time)].';
H = bar(time_vec(sub_time), total_camera_obs, 'stacked');
colors = [0, 0.75, 0; 0.75, 0, 0.75];
for k = 1:2
    set(H(k), 'facecolor', colors(k,:), 'EdgeColor', 'none');
end
% xlabel({'(c)';'Time (s)'});
xlabel('(c)');
ylabel('Observations');
legend('Landmarks', 'Players'); ylim([0,8]); xlim(xlim_param + [0, T]);
grid on;

subplot(24,1,22:24);
plot(time_vec_obscamera, 3*T*maxBlackoutLength(r1,:));
xlabel({'(d)';'Time (s)'}); ylabel('Max. blackout (s)'); ylim([0,30]);
grid on;

if saveplots
    saveas(gcf, 'figures_temp/res_fig5.fig'), saveas(gcf, 'figures_temp/res_fig5.png'), saveas(gcf, 'figures_temp/res_fig5','epsc');
end

% PLOT 6: caso menos favor�vel para coopera��o: rob�s que veem muitos landmarks (1, 4 ou 5)
figure(6)
r1 = 5;
% subplot(18,1,1:6);
subplot(24,1,1:6);
for s = s_vec
    mean_mse = mean( mse_dist{r1,s}, 1);
    var_mse = var( mse_dist{r1,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
    plot(time_vec(sub_time), sqrt(mean_mse(sub_time)), ':', 'LineWidth', 1.5);
    hold on;
end
grid on;
legend(['Robot ' num2str(r1) ' - ' setupNamePlot{1}], ['Robot ' num2str(r1) ' - ' setupNamePlot{4}]);
% xlabel('Time (s)'); 
ylabel('Position RMSE (m)');
xlabel('(a)');
xlim(xlim_param); ylim(ylim_dist_param)

% subplot(18,1,8:13);
subplot(24,1,9:14);
for s = s_vec
    mean_mse = mean( mse_psi{r1,s}, 1);
    var_mse = var( mse_psi{r1,s}, 0, 1);
    ax = gca;
    ax.ColorOrderIndex = s;
    plot(time_vec(sub_time), sqrt(mean_mse(sub_time)), ':', 'LineWidth', 1.5);
%     semilogy(time_vec(sub_time), sqrt(mean_mse(sub_time)), [linetype{r1}], 'LineWidth', 1.5);
    hold on;
end
grid on;
legend(['Robot ' num2str(r1) ' - ' setupNamePlot{1}], ['Robot ' num2str(r1) ' - ' setupNamePlot{4}]);
% xlabel('Time (s)'); 
ylabel('Orientation RMSE (rad)');
xlabel('(b)');
xlim(xlim_param); ylim(ylim_psi_param);

% subplot(18,1,15:18);
subplot(24,1,17:19);
total_camera_obs = [obs_landmarks_average(r1,sub_time); obs_players_average(r1,sub_time)].';
H = bar(time_vec(sub_time), total_camera_obs, 'stacked');
colors = [0, 0.75, 0; 0.75, 0, 0.75];
for k = 1:2
    set(H(k), 'facecolor', colors(k,:), 'EdgeColor', 'none');
end
% xlabel({'(c)';'Time (s)'});
xlabel('(c)');
ylabel('Observations');
legend('Landmarks', 'Players'); ylim([0,8]); xlim(xlim_param + [0, T]);
grid on;

subplot(24,1,22:24)
plot(time_vec_obscamera, 3*T*maxBlackoutLength(r1,:));
xlabel({'(d)';'Time (s)'}); ylabel('Max. blackout (s)'); ylim([0,30]);
grid on;

if saveplots
    saveas(gcf, 'figures_temp/res_fig6.fig'), saveas(gcf, 'figures_temp/res_fig6.png'), saveas(gcf, 'figures_temp/res_fig6','epsc');
end

%% Scattering error and number of observations
bPlotScatter = false;
if bPlotScatter
% single value for the error for each robot: er_psi_metodo
% single value for average of landmarks and players
% *3 factor corresponds to the camera observations being taken every 3 time
% instants
colors = [0, 0, 1;
          1, 0, 0;
          1, 1, 0;
          1, 0, 1];
obs_landmarks_r = mean(obs_landmarks_average,2)*3;
obs_players_r = mean(obs_players_average,2)*3;
obs_objects_r = obs_landmarks_r + obs_players_r;

num_hearing_r = (R-1)/(2*R)*ones(5,1);

% Average number of landmarks (considering only the corresponding time instants)
figure(71);
subplot(2,2,1);
for s = 1:4
    scatter(obs_landmarks_r, er_dist_metodo(s,:), 30, 'MarkerFaceColor', colors(s,:));
    hold on;
end
xlabel('Time average of observed landmarks'); ylabel('Position RMSE (m)');

subplot(2,2,2);
for s = 1:4
    scatter(mean(obs_landmarks_r), mean(er_dist_metodo(s,:)), 30, 'MarkerFaceColor', colors(s,:));
    hold on;
end
xlabel('Time average of observed landmarks'); ylabel('Position RMSE (m)');

subplot(2,2,3);
for s = 1:4
    scatter(obs_landmarks_r, er_psi_metodo(s,:), 30, 'MarkerFaceColor', colors(s,:));
    hold on;
end
xlabel('Time average of observed landmarks'); ylabel('Orientation RMSE (m)');

subplot(2,2,4);
for s = 1:4
    scatter(mean(obs_landmarks_r), mean(er_psi_metodo(s,:)), 30, 'MarkerFaceColor', colors(s,:));
    hold on;
end
xlabel('Time average of observed landmarks'); ylabel('Orientation RMSE (m)');

% Average landmark seen (considering only the corresponding time instants)
% for PL and PLH
% Average landmark plus players for PLA and PLHA
figure(72);
subplot(2,2,1);
for s = 1:4
    if s == 1 || s == 2
        scatter(obs_landmarks_r, er_dist_metodo(s,:), 30, 'MarkerFaceColor', colors(s,:));
    else
        scatter(obs_objects_r, er_dist_metodo(s,:), 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Time average of observed objects'); ylabel('Position RMSE (m)');

subplot(2,2,2);
for s = 1:4
    if s == 1 || s == 2
        scatter(mean(obs_landmarks_r), mean(er_dist_metodo(s,:)), 30, 'MarkerFaceColor', colors(s,:));
    else
        scatter(mean(obs_objects_r), mean(er_dist_metodo(s,:)), 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Time average of observed objects'); ylabel('Position RMSE (m)');

subplot(2,2,3);
for s = 1:4
    if s == 1 || s == 2
        scatter(obs_landmarks_r, er_psi_metodo(s,:), 30, 'MarkerFaceColor', colors(s,:));
    else
        scatter(obs_objects_r, er_psi_metodo(s,:), 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Time average of observed objects'); ylabel('Orientation RMSE (m)');

subplot(2,2,4);
for s = 1:4
    if s == 1 || s == 2
        scatter(mean(obs_landmarks_r), mean(er_psi_metodo(s,:)), 30, 'MarkerFaceColor', colors(s,:));
    else
        scatter(mean(obs_objects_r), mean(er_psi_metodo(s,:)), 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Time average of observed objects'); ylabel('Orientation RMSE (m)');

% Measurements: each camera observation corresponds to one measurement; 
% each DoA corresponds to one.
figure(73);
subplot(2,2,1);
for s = 1:4
    err = er_dist_metodo(s,:);
    if s == 1
        scatter(obs_landmarks_r/3, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 2
        scatter(obs_landmarks_r/3 + num_hearing_r, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 3
        scatter(obs_objects_r/3, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 4
        scatter(obs_objects_r/3 + num_hearing_r, err, 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Average of measurements'); 
ylabel('Position RMSE (m)');

subplot(2,2,2);
for s = 1:4
    err = mean(er_dist_metodo(s,:));
    if s == 1
        scatter(mean(obs_landmarks_r/3), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 2
        scatter(mean(obs_landmarks_r/3 + num_hearing_r), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 3
        scatter(mean(obs_objects_r/3), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 4
        scatter(mean(obs_objects_r/3 + num_hearing_r), err, 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Average of measurements'); 
ylabel('Position RMSE (m)');

subplot(2,2,3);
for s = 1:4
    err = er_psi_metodo(s,:);
    if s == 1
        scatter(obs_landmarks_r/3, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 2
        scatter(obs_landmarks_r/3 + num_hearing_r, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 3
        scatter(obs_objects_r/3, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 4
        scatter(obs_objects_r/3 + num_hearing_r, err, 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Average of measurements'); 
ylabel('Orientation RMSE (m)');

subplot(2,2,4);
for s = 1:4
    err = mean(er_psi_metodo(s,:));
    if s == 1
        scatter(mean(obs_landmarks_r/3), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 2
        scatter(mean(obs_landmarks_r/3 + num_hearing_r), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 3
        scatter(mean(obs_objects_r/3), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 4
        scatter(mean(obs_objects_r/3 + num_hearing_r), err, 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Average of measurements'); 
ylabel('Orientation RMSE (m)');

% Scalar measurements: each camera observation corresponds to two scalar
% measurements; each DoA corresponds to one.
figure(74);
subplot(2,2,1);
for s = 1:4
    err = er_dist_metodo(s,:);
    if s == 1
        scatter(obs_landmarks_r*2/3, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 2
        scatter(obs_landmarks_r*2/3 + num_hearing_r, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 3
        scatter(obs_objects_r*2/3, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 4
        scatter(obs_objects_r*2/3 + num_hearing_r, err, 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Average of scalar measurements');
ylabel('Position RMSE (m)');

subplot(2,2,2);
for s = 1:4
    err = mean(er_dist_metodo(s,:));
    if s == 1
        scatter(mean(obs_landmarks_r*2/3), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 2
        scatter(mean(obs_landmarks_r*2/3 + num_hearing_r), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 3
        scatter(mean(obs_objects_r*2/3), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 4
        scatter(mean(obs_objects_r*2/3 + num_hearing_r), err, 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Average of scalar measurements');
ylabel('Position RMSE (m)');

subplot(2,2,3);
for s = 1:4
    err = er_psi_metodo(s,:);
    if s == 1
        scatter(obs_landmarks_r*2/3, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 2
        scatter(obs_landmarks_r*2/3 + num_hearing_r, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 3
        scatter(obs_objects_r*2/3, err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 4
        scatter(obs_objects_r*2/3 + num_hearing_r, err, 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Average of scalar measurements');
ylabel('Orientation RMSE (m)');

subplot(2,2,4);
for s = 1:4
    err = mean(er_psi_metodo(s,:));
    if s == 1
        scatter(mean(obs_landmarks_r*2/3), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 2
        scatter(mean(obs_landmarks_r*2/3 + num_hearing_r), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 3
        scatter(mean(obs_objects_r*2/3), err, 30, 'MarkerFaceColor', colors(s,:));
    elseif s == 4
        scatter(mean(obs_objects_r*2/3 + num_hearing_r), err, 30, 'MarkerFaceColor', colors(s,:));
    end
    hold on;
end
xlabel('Average of scalar measurements');
ylabel('Orientation RMSE (m)');

end

%% Plotting only one method (for P)
% figure(11);
% plot(time_vec(sub_time), sqrt(en_dist_metodo(1,sub_time)), linetype{1}, 'LineWidth', 1.5);
% H = gca;
% grid on;
% legend('P');
% xlabel('Time (s)'); ylabel('Position RMSE (m)');
% 
% 
% figure(12);
% bar(sqrt(er_dist_metodo(1:2,:))); ylim(ylim_dist_param);
% ylabel('Position RMSE (m)'); xlabel('Variant')
% legend('Robot 1', 'Robot 2', 'Robot 3', 'Robot 4', 'Robot 5');
% 
% 
% figure(13);
% plot(time_vec(sub_time), sqrt(en_psi_metodo(1,sub_time)), linetype{1}, 'LineWidth', 1.5);
% grid on;
% legend('P');
% xlabel('Time (s)'); ylabel('Orientation RMSE (rad)'); 
