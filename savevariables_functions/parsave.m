% function for saving the "save struct", whose fields contain all desired
% variables, in a .mat file. It uses the built-in save function.
% Inputs:
% - filename: string containing the desired name of the file
% - savedvariable: variable to be saved, which contains all desired variables
function parsave(filename, savedvariable)
  save(filename, 'savedvariable');
end