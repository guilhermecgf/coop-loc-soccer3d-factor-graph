---------------------------------------------
Executing the simulation

- Run simulateMultiplayerMonteCarloTrajectory.m
- Methods for scenario generation and estimation are in the class Humanoid.m

---------------------------------------------
Evaluating the results

- 1st step (possibly not needed): in output_final/rename_files.m, rename files with different IDs to a same ID;

- 2nd step: processAllMatFiles.m: opens all .mat files with a given ID number, saves into a single simID.mat file.

- 3rd step: plotsPaper.m and plotsAllRobotsAndMonteCarlo.m

- 4th step: plotCpuTime.m