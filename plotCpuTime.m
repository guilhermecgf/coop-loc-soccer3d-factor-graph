%% Script to plot the mean time of each algorithm
clear all; close all

addpath('plotresults_functions')
% load('output_final/sim29/sim29.mat'); %sim29: 400 particles
% load('output_final/sim41/sim41.mat'); %sim41: 100 particles
% load('output_final/sim382.mat'); %sim41: 100 particles
% load('D:\OneDrive\sim73.mat'); % finais paper, Np = 300, most recent belief = false
load('D:\OneDrive\sim107.mat'); % finais paper, Np = 200, most recent belief = false

%% 
avg_cputime_per_robot_mc = mean(avg_cputime_per_robot_allmc,3);

avg_landmarks_total = squeeze(mean(mean(obs_landmarks_total,1),2));
avg_players_total = squeeze(mean(mean(obs_players_total,1),2));
mean(avg_landmarks_total(3:3:end))
mean(avg_players_total(3:3:end))

%% PL and variants
% Distinguishing the different cases on PL assimilation
PL_PL_ind = [];
PL_P_ind = [];

% Come�ando em 2, pois em 1 n�o houve processamento

for i = 2:nIterations
    if mod(i,3) == 0
        PL_PL_ind = [PL_PL_ind, i];
    else
        PL_P_ind = [PL_P_ind, i];
    end
end

avg_cputime_per_robot_mc_PL_PL = avg_cputime_per_robot_mc(1,PL_PL_ind);
avg_cputime_per_robot_mc_PL_P = avg_cputime_per_robot_mc(1,PL_P_ind);

PL_avg_cputime = cell(2,3);
PL_avg_cputime{1,1} = 'PL_PL';
PL_avg_cputime{1,2} = mean(avg_cputime_per_robot_mc_PL_PL);
PL_avg_cputime{1,3} = std(avg_cputime_per_robot_mc_PL_PL);
PL_avg_cputime{2,1} = 'PL_P';
PL_avg_cputime{2,2} = mean(avg_cputime_per_robot_mc_PL_P);
PL_avg_cputime{2,3} = std(avg_cputime_per_robot_mc_PL_P);

%% PLH and variants
PLH_PLH_ind = [];
PLH_PL_ind = [];
PLH_PH_ind = [];
PLH_P_ind = [];

for i = 1:nIterations
    if mod(i,6) == 0
        PLH_PLH_ind = [PLH_PLH_ind, i];
    elseif mod(i,3) == 0
        PLH_PL_ind = [PLH_PL_ind, i];
    elseif mod(i,2) == 0
        PLH_PH_ind = [PLH_PH_ind, i];
    else
        PLH_P_ind = [PLH_P_ind, i];
    end
end

avg_cputime_per_robot_mc_PLH_PLH = avg_cputime_per_robot_mc(2,PLH_PLH_ind);
avg_cputime_per_robot_mc_PLH_PL = avg_cputime_per_robot_mc(2,PLH_PL_ind);
avg_cputime_per_robot_mc_PLH_PH = avg_cputime_per_robot_mc(2,PLH_PH_ind);
avg_cputime_per_robot_mc_PLH_P = avg_cputime_per_robot_mc(2,PLH_P_ind);

PLH_avg_cputime = cell(4,3);
PLH_avg_cputime{1,1} = 'PLH_PLH';
PLH_avg_cputime{1,2} = mean(avg_cputime_per_robot_mc_PLH_PLH);
PLH_avg_cputime{1,3} = std(avg_cputime_per_robot_mc_PLH_PLH);
PLH_avg_cputime{2,1} = 'PLH_PL';
PLH_avg_cputime{2,2} = mean(avg_cputime_per_robot_mc_PLH_PL);
PLH_avg_cputime{2,3} = std(avg_cputime_per_robot_mc_PLH_PL);
PLH_avg_cputime{3,1} = 'PLH_PH';
PLH_avg_cputime{3,2} = mean(avg_cputime_per_robot_mc_PLH_PH);
PLH_avg_cputime{3,3} = std(avg_cputime_per_robot_mc_PLH_PH);
PLH_avg_cputime{4,1} = 'PLH_P';
PLH_avg_cputime{4,2} = mean(avg_cputime_per_robot_mc_PLH_P);
PLH_avg_cputime{4,3} = std(avg_cputime_per_robot_mc_PLH_P);

%% PLHA and variants
PLHA_PLHA_ind = [];
PLHA_PLA_ind = [];
PLHA_PH_ind = [];
PLHA_P_ind = [];

for i = 1:nIterations
    if mod(i,6) == 0
        PLHA_PLHA_ind = [PLHA_PLHA_ind, i];
    elseif mod(i,3) == 0
        PLHA_PLA_ind = [PLHA_PLA_ind, i];
    elseif mod(i,2) == 0
        PLHA_PH_ind = [PLHA_PH_ind, i];
    else
        PLHA_P_ind = [PLHA_P_ind, i];
    end
end

avg_cputime_per_robot_mc_PLHA_PLHA = avg_cputime_per_robot_mc(3,PLHA_PLHA_ind);
avg_cputime_per_robot_mc_PLHA_PLA = avg_cputime_per_robot_mc(3,PLHA_PLA_ind);
avg_cputime_per_robot_mc_PLHA_PH = avg_cputime_per_robot_mc(3,PLHA_PH_ind);
avg_cputime_per_robot_mc_PLHA_P = avg_cputime_per_robot_mc(3,PLHA_P_ind);

PLHA_avg_cputime = cell(4,3);
PLHA_avg_cputime{1,1} = 'PLHA_PLHA';
PLHA_avg_cputime{1,2} = mean(avg_cputime_per_robot_mc_PLHA_PLHA);
PLHA_avg_cputime{1,3} = std(avg_cputime_per_robot_mc_PLHA_PLHA);
PLHA_avg_cputime{2,1} = 'PLHA_PLA';
PLHA_avg_cputime{2,2} = mean(avg_cputime_per_robot_mc_PLHA_PLA);
PLHA_avg_cputime{2,3} = std(avg_cputime_per_robot_mc_PLHA_PLA);
PLHA_avg_cputime{3,1} = 'PLHA_PH';
PLHA_avg_cputime{3,2} = mean(avg_cputime_per_robot_mc_PLHA_PH);
PLHA_avg_cputime{3,3} = std(avg_cputime_per_robot_mc_PLHA_PH);
PLHA_avg_cputime{4,1} = 'PLHA_P';
PLHA_avg_cputime{4,2} = mean(avg_cputime_per_robot_mc_PLHA_P);
PLHA_avg_cputime{4,3} = std(avg_cputime_per_robot_mc_PLHA_P);

%% PLA and variants
PLA_PLA_ind = [];
PLA_P_ind = [];

for i = 1:nIterations
    if mod(i,3) == 0
        PLA_PLA_ind = [PLA_PLA_ind, i];
    else
        PLA_P_ind = [PLA_P_ind, i];
    end
end

avg_cputime_per_robot_mc_PLA_PLA = avg_cputime_per_robot_mc(4,PLA_PLA_ind);
avg_cputime_per_robot_mc_PLA_P = avg_cputime_per_robot_mc(4,PLA_P_ind);

PLA_avg_cputime = cell(2,3);
PLA_avg_cputime{1,1} = 'PLA_PLA';
PLA_avg_cputime{1,2} = mean(avg_cputime_per_robot_mc_PLA_PLA);
PLA_avg_cputime{1,3} = std(avg_cputime_per_robot_mc_PLA_PLA);
PLA_avg_cputime{2,1} = 'PLA_P';
PLA_avg_cputime{2,2} = mean(avg_cputime_per_robot_mc_PLA_P);
PLA_avg_cputime{2,3} = std(avg_cputime_per_robot_mc_PLA_P);

%%
PL_avg_cputime;
PL_avg = mean(avg_cputime_per_robot_mc(1,2:end));
PL_std = std(avg_cputime_per_robot_mc(1,2:end));
PLH_avg_cputime;
PLH_avg = mean(avg_cputime_per_robot_mc(2,2:end));
PLH_std = std(avg_cputime_per_robot_mc(2,2:end));
PLHA_avg_cputime;
PLHA_avg = mean(avg_cputime_per_robot_mc(3,2:end));
PLHA_std = std(avg_cputime_per_robot_mc(3,2:end));
PLA_avg_cputime;
PLA_avg = mean(avg_cputime_per_robot_mc(4,2:end));
PLA_std = std(avg_cputime_per_robot_mc(4,2:end));

methods = cell(4,3);
methods{1,1} = 'PL'; methods{1,2} = PL_avg; methods{1,3} = PL_std;
methods{2,1} = 'PLH'; methods{2,2} = PLH_avg; methods{2,3} = PLH_std;
methods{3,1} = 'PLA'; methods{3,2} = PLA_avg; methods{3,3} = PLA_std;
methods{4,1} = 'PLHA'; methods{4,2} = PLHA_avg; methods{4,3} = PLHA_std;
methods

%%
all_P_avg_cputime = [avg_cputime_per_robot_mc_PL_P, ...
                     avg_cputime_per_robot_mc_PLH_P, ...
                     avg_cputime_per_robot_mc_PLHA_P, ...
                     avg_cputime_per_robot_mc_PLA_P];
                 

all_PL_avg_cputime = [avg_cputime_per_robot_mc_PL_PL, ...
                      avg_cputime_per_robot_mc_PLH_PL];
                 

all_PH_avg_cputime = [avg_cputime_per_robot_mc_PLH_PH, ...
                      avg_cputime_per_robot_mc_PLHA_PH,];
                 

all_PLH_avg_cputime = [avg_cputime_per_robot_mc_PLH_PLH];
                 

all_PLA_avg_cputime = [avg_cputime_per_robot_mc_PLHA_PLA, ...
                       avg_cputime_per_robot_mc_PLA_PLA];
                 

all_PLHA_avg_cputime = [avg_cputime_per_robot_mc_PLHA_PLHA];

avg_cputime = cell(6,3);
avg_cputime{1,1} = 'P';
avg_cputime{1,2} = mean(all_P_avg_cputime);
avg_cputime{1,3} = std(all_P_avg_cputime);
avg_cputime{2,1} = 'PL';
avg_cputime{2,2} = mean(all_PL_avg_cputime);
avg_cputime{2,3} = std(all_PL_avg_cputime);
avg_cputime{3,1} = 'PH';
avg_cputime{3,2} = mean(all_PH_avg_cputime);
avg_cputime{3,3} = std(all_PH_avg_cputime);
avg_cputime{4,1} = 'PLH';
avg_cputime{4,2} = mean(all_PLH_avg_cputime);
avg_cputime{4,3} = std(all_PLH_avg_cputime);
avg_cputime{5,1} = 'PLA';
avg_cputime{5,2} = mean(all_PLA_avg_cputime);
avg_cputime{5,3} = std(all_PLA_avg_cputime);
avg_cputime{6,1} = 'PLHA';
avg_cputime{6,2} = mean(all_PLHA_avg_cputime);
avg_cputime{6,3} = std(all_PLHA_avg_cputime);

avg_cputime

%% Normalizing
methods_norm = methods;
avg_cputime_norm = avg_cputime;
norm_factor = methods{1,2};

for s = 1:4
    for col = 2:3
        methods_norm{s,col} = methods{s,col}/norm_factor;
    end
end

for comb = 1:6
    for col = 2:3
        avg_cputime_norm{comb,col} = avg_cputime{comb,col}/norm_factor;
    end
end

methods_norm
avg_cputime_norm

PL_period = [avg_cputime_norm{1,2}, avg_cputime_norm{1,2}, avg_cputime_norm{2,2}, ...
             avg_cputime_norm{1,2}, avg_cputime_norm{1,2}, avg_cputime_norm{2,2}; ...
             avg_cputime_norm{1,3}, avg_cputime_norm{1,3}, avg_cputime_norm{2,3}, ...
             avg_cputime_norm{1,3}, avg_cputime_norm{1,3}, avg_cputime_norm{2,3}];

PLH_period = [avg_cputime_norm{1,2}, avg_cputime_norm{3,2}, avg_cputime_norm{2,2}, ...
             avg_cputime_norm{3,2}, avg_cputime_norm{1,2}, avg_cputime_norm{4,2}; ...
             avg_cputime_norm{1,3}, avg_cputime_norm{3,3}, avg_cputime_norm{2,3}, ...
             avg_cputime_norm{3,3}, avg_cputime_norm{1,3}, avg_cputime_norm{4,3}];

PLA_period = [avg_cputime_norm{1,2}, avg_cputime_norm{1,2}, avg_cputime_norm{5,2}, ...
             avg_cputime_norm{1,2}, avg_cputime_norm{1,2}, avg_cputime_norm{5,2}; ...
             avg_cputime_norm{1,3}, avg_cputime_norm{1,3}, avg_cputime_norm{5,3}, ...
             avg_cputime_norm{1,3}, avg_cputime_norm{1,3}, avg_cputime_norm{5,3}];

PLHA_period = [avg_cputime_norm{1,2}, avg_cputime_norm{3,2}, avg_cputime_norm{5,2}, ...
             avg_cputime_norm{3,2}, avg_cputime_norm{1,2}, avg_cputime_norm{6,2}; ...
             avg_cputime_norm{1,3}, avg_cputime_norm{3,3}, avg_cputime_norm{5,3}, ...
             avg_cputime_norm{3,3}, avg_cputime_norm{1,3}, avg_cputime_norm{6,3}];
         
figure;
subplot(2,2,1);
PL_combs = {'P', 'P', 'P&L', 'P', 'P', 'P&L'};
errorbar(1:6, PL_period(1,:), PL_period(2,:), ':o');
xlabel('Discrete time instant');
ylabel('Normalized average CPU time');
for i = 1:6
    text(i,PL_period(1,i), ['\leftarrow ' PL_combs{i}]);
end
ylim([0,6]);

subplot(2,2,2);
PLH_combs = {'P', 'P&H', 'P&L', 'P&H', 'P', 'P&L&H'};
errorbar(1:6, PLH_period(1,:), PLH_period(2,:), ':o');
xlabel('Discrete time instant');
ylabel('Normalized average CPU time');
for i = 1:6
    text(i,PLH_period(1,i), ['\leftarrow ' PLH_combs{i}]);
end
ylim([0,6]);

subplot(2,2,3);
PLA_combs = {'P', 'P', 'P&L&A', 'P', 'P', 'P&L&A'};
errorbar(1:6, PLA_period(1,:), PLA_period(2,:), ':o');
xlabel('Discrete time instant');
ylabel('Normalized average CPU time');
for i = 1:6
    text(i,PLA_period(1,i), ['\leftarrow ' PLA_combs{i}]);
end
ylim([0,85]);

subplot(2,2,4);
PLHA_combs = {'P', 'P&H', 'P&L&A', 'P&H', 'P', 'P&L&H&A'};
errorbar(1:6, PLHA_period(1,:), PLHA_period(2,:), ':o');
xlabel('Discrete time instant');
ylabel('Normalized average CPU time');
for i = 1:6
    text(i,PLHA_period(1,i), ['\leftarrow ' PLHA_combs{i}]);
end
ylim([0,85]);
