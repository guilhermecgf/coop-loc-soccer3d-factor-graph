%% --------------------------------------------------

addpath('output_final');
newID = 0;
ID_vec = [0,10,30,50,70,90,110,130,150,170];
rename_files(newID, ID_vec);

%%
% Script to open all .mat files and save everything into a single file
% clear all; close all
% Second set of simulations: from BU cluster
% modifications: T = 0.02, smaller noise variances, less total time, no
% noise on input sequence (not odometry anymore, but deterministic input

setup = [true, true, false, false; 
         true, true, true, false; 
         true, true, true, true; 
         true, true, false, true];
setupName = {'PL', 'PLH', 'PLHA', 'PLA'};

% obs_landmarks_total = [];
% obs_players_total = [];
% obs_allies_detail = [];
% 
% err_x_time_total = [];
% err_y_time_total = [];
% err_psi_time_total = [];
% 
% mse_x_time_total = [];
% mse_y_time_total = [];
% mse_psi_time_total = [];
% mse_dist_time_total = [];

data = cell(1,4);

% % teste apenas PL
% % setupName = {'PL', 'PL', 'PL', 'PL'};
% setupName = {'P', 'P', 'P', 'P'};
% ID = 641;
% M = 10;
% strbase = ['output_final/' num2str(ID)];
% 
% ID = 284;
% M = 76;
% strbase = ['output_final/' num2str(ID)];

% teste de todos
setupName = {'PL', 'PLH', 'PLHA', 'PLA'};
% ID = newID;
% M = 100;
ID = 0;
M = 10;
strbase = ['output_final/' num2str(ID)];


for m = 1:M
    for s = 1:4
        filename = [strbase '_' setupName{s} '_MCtraj' num2str(m,'%03d') '.mat'];
        disp(filename);
        aux = load(filename);
        data{s} = aux.savedvariable;
    end     
        
%     % APENAS PARA O TESTE S� DE PL
%     for s = 2:4
%         R = data{1}.simConfig.R;
%         for r = 1:R
%             data{s}.TeamEstSave{r,s} = data{1}.TeamEstSave{r,1};
%         end
%     end
%     % COMENTAR DEPOIS
    
    if m == 1
        R = data{1}.simConfig.R;
        nIterations = data{1}.simConfig.nIterations;
        T = data{1}.simConfig.T;
        time_vec = T * [1:nIterations];

        obs_landmarks = zeros(M, R, nIterations);
        obs_players = zeros(M, R, nIterations);
        obs_allies_detail = zeros(M, R, R, nIterations);
        
        gt_x_time = zeros(M, nIterations, R);
        gt_y_time = zeros(M, nIterations, R);
        gt_psi_time = zeros(M, nIterations, R);
        
        est_x_time = zeros(M, nIterations, R, 4);
        est_y_time = zeros(M, nIterations, R, 4);
        est_psi_time = zeros(M, nIterations, R, 4);
        
        cov_time = zeros(3, 3, nIterations, M, R, 4);
        Neff_time = zeros(M, nIterations, R, 4);

        err_x_time = zeros(M, nIterations, R, 4);
        err_y_time = zeros(M, nIterations, R, 4);
        err_psi_time = zeros(M, nIterations, R, 4);

        mse_x_time = zeros(M, nIterations, R, 4);
        mse_y_time = zeros(M, nIterations, R, 4);
        mse_psi_time = zeros(M, nIterations, R, 4);
        mse_dist_time = zeros(M, nIterations, R, 4);
        
        avg_cputime_per_robot_allmc = zeros(4, nIterations, M);

        
    end 

    for r=1:R
        for i=2:nIterations
            try
                obs_landmarks(m,r,i) = length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.goalsID) + length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.cornersID);
                obs_players(m,r,i) = length(data{1}.TeamEstSave{r,1}.measurementCamera{i}.playersID);
                
                detectedAllies = data{1,4}.TeamEstSave{r,1}.measurementCamera{i}.playersID;
                for d = detectedAllies
                    obs_allies_detail(m,r,d,i) = 1;
                end

            catch
                %disp(['no camera observations at time instant ' num2str(i)]);
            end
        end
    end

    for r = 1:R
        
        gt_x_time(m, :, r) = data{1}.TeamEstSave{r,1}.realData(:,1);
        gt_y_time(m, :, r) = data{1}.TeamEstSave{r,1}.realData(:,2);
        gt_psi_time(m, :, r) = data{1}.TeamEstSave{r,1}.realData(:,3);
        
        for s = 1:4
            
            Neff_time(m, : , r, s) = (data{s}.TeamEstSave{r,s}.recordNeff(1,:));
            
            est_x = data{s}.TeamEstSave{r,s}.estimatedState(:,1);
            est_x_time(m, :, r, s) = est_x;
            
            est_y = data{s}.TeamEstSave{r,s}.estimatedState(:,1);
            est_y_time(m, :, r, s) = est_y;
            
            est_psi = data{s}.TeamEstSave{r,s}.estimatedState(:,1);
            est_psi_time(m, :, r, s) = est_psi;
            
            cov_time(:,:,:,m,r,s) = data{s}.TeamEstSave{r,s}.estimatedCovariance;
                        
            err_x = data{s}.TeamEstSave{r,s}.estimationError(1,:);
            err_x_time(m, :, r, s) = err_x;
            mse_x_time(m, :, r, s) = err_x.^2;

            err_y = data{s}.TeamEstSave{r,s}.estimationError(2,:);
            err_y_time(m, :, r, s) = err_y;
            mse_y_time(m, :, r, s) = err_y.^2;

            mse_dist_time(m, :, r, s) = mse_x_time(m, :, r, s) + mse_y_time(m, :, r, s);

            err_psi = data{s}.TeamEstSave{r,s}.estimationError(3,:);
            err_psi_time(m, :, r, s) = err_psi;
            mse_psi_time(m, :, r, s) = err_psi.^2;
        end
    end
    
    for s = 1:4
        avg_cputime_per_robot_allmc(s,:,m) = data{s}.avg_cputime_per_robot(s,:);
    end
    
end

% reducing the size, since entry 4 contains all information in entries 1 to
% 3. Actually, data only contains information about the last MC realization
data = data{4};

obs_landmarks_total = obs_landmarks; clear obs_landmarks
obs_players_total = obs_players; clear obs_players

err_x_time_total = err_x_time; clear err_x_time
err_y_time_total = err_y_time; clear err_y_time
err_psi_time_total = err_psi_time; clear err_psi_time

mse_x_time_total = mse_x_time; clear mse_x_time
mse_y_time_total = mse_y_time; clear mse_y_time
mse_psi_time_total = mse_psi_time; clear mse_psi_time
mse_dist_time_total = mse_dist_time; clear mse_dist_time

% save(['output_final/sim_cluster_bu/sim_cluster_bu' num2str(ID) '.mat']);
% save(['output_final/sim_itandroids_5/sim' num2str(ID) '.mat']);
save(['output_final/sim' num2str(ID) '.mat']);